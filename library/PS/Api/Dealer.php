<?php 
class PS_Api_Dealer extends PS_Controller_Action{
	
	 function init()
	{echo "hii";exit;
         parent::init();
		 $actionName     = $this->getRequest()->getActionName();
         $controllerName = $this->getRequest()->getControllerName();
		 $this->view->actionName = $actionName;
		 $this->view->controllerName = $controllerName;
	}
	
    // define filters and validators for input
    private $_filters = array(
      'title'     => array('HtmlEntities', 'StripTags', 'StringTrim'),
      'shortdesc' => array('HtmlEntities', 'StripTags', 'StringTrim'),
      'price'     => array('HtmlEntities', 'StripTags', 'StringTrim'),
      'quantity'  => array('HtmlEntities', 'StripTags', 'StringTrim')
    );
    
    private $_validators = array(
      'title'     => array(),
      'shortdesc' => array(),
      'price'     => array('Float'),
      'quantity'  => array('Int')
    );
  
    /**
     * Returns list of all products in database
     *
     * @return array
     */
    
    public function getProducts() 
    { 
      echo "hii";exit;
	  	$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
	 	$this->view->siteTitle = $objTranslate->translate('LABEL_DEALER_MANAGEMENT');
        $objError = new Zend_Session_Namespace(PS_App_Error);
        $objSess = new Zend_Session_Namespace(PS_App_Auth);
        $usertype =   $objSess->user_type;
      	$this->view->message = $objError->message;
      	$this->view->messageType = $objError->messageType;
		$objError->message = "" ;
		$objError->messageType = '';
	 	$objModel = new Models_Dealer();

	 	$objSelect = $objModel->getAllDealerList();	
    	
      $db = Zend_Registry::get('Zend_Db');        
      $sql = "SELECT * FROM products";      
      return $db->fetchAll($sql);      
    }
    
    /**
     * Returns specified product in database
     *
     * @param integer $id
     * @return array|Example_Exception
     */
    public function getProduct($id) 
    {
      if (!Zend_Validate::is($id, 'Int')) {
        throw new Example_Exception('Invalid input');          
      }
      $db = Zend_Registry::get('Zend_Db');        
      $sql = "SELECT * FROM products WHERE id = '$id'";   
      $result = $db->fetchAll($sql);      
      if (count($result) != 1) {        
        throw new Example_Exception('Invalid product ID: ' . $id);  
      } 
      return $result;  
    }
    
    /**
     * Adds new product to database
     *
     * @param array $data array of data values with keys -> table fields
     * @return integer id of inserted product
     */
    public function addProduct($data) 
    {  //print_r($data);exit;
      $input = new Zend_Filter_Input($this->_filters, $this->_validators, $data);
      if (!$input->isValid()) {
        throw new Example_Exception('Invalid input');  
      }
      $values = $input->getEscaped();
      $db = Zend_Registry::get('Zend_Db');        
      $db->insert('products', $values);
      return $db->lastInsertId();
    }    

    /**
     * Deletes product from database
     *
     * @param integer $id
     * @return integer number of products deleted
     */
    public function deleteProduct($id) 
    { 
      if (!Zend_Validate::is($id, 'Int')) {
        throw new Example_Exception('Invalid input');          
      }
      $db = Zend_Registry::get('Zend_Db');        
      $count = $db->delete('products', 'id=' . $db->quote($id));
      return $count;
    }    

    /**
     * Updates product in database
     *
     * @param integer $id
     * @param array $data
     * @return integer number of products updated
     */
    public function updateProduct($id, $data) 
    {
      $input = new Zend_Filter_Input($this->_filters, $this->_validators, $data);
      if (!Zend_Validate::is($id, 'Int') || !$input->isValid()) {
        throw new Example_Exception('Invalid input');          
      } 
      $values = $input->getEscaped();
      $db = Zend_Registry::get('Zend_Db');        
      $count = $db->update('products', $values, 'id=' . $db->quote($id));
      return $count;        
    }    
            
}

