<?php
class PS_Controller_Action extends Zend_Controller_Action {
	
	private $_extraVar = array();
	private $_siteVar = array();
	public 	$objSess = array();
	
    function init() {
    	/*Script Added on 12-12-2012*/
    	$objGeneralModel = new Models_Generalsettings();
        $objGeneralSelect = $objGeneralModel->getGeneralSettings();
	
		foreach($objGeneralSelect as $key=>$val) {
			$this->_siteVar[$key] = $val;
		}
		/*End Added*/
		
		/*Script commented on 12-12-2012*/	
		/*
		$objGeneralModel = new Models_GeneralSetting();
        $objGeneralSelect = $objGeneralModel->getGeneralSettingRec();
		foreach($objGeneralSelect as $GeneralSettings) {
			$this->_siteVar[$GeneralSettings['settings_varname']] = $GeneralSettings['settings_value'];
		}						
		*/
		
		$this->view->siteTitle = self::getSiteVar('SITE_TITLE');

        $layout 		= Zend_Layout::getMvcInstance();
        $moduleName     = $this->getRequest()->getModuleName();
        $actionName     = $this->getRequest()->getActionName();
        $controllerName = $this->getRequest()->getControllerName();
		
        $this->view->siteTitle = self::getSiteVar('SITE_TITLE')." - $controllerName - $actionName";
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		$this->_extraVar = array('controller'=>$controllerName,'action'=>$actionName,'module'=>$moduleName);
		
		$this->view->headerFile = 'header.phtml';		
		$this->view->leftFile = 'left.phtml';		
		$this->view->footerFile = 'footer.phtml';				
		
		
		//change application language 
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objSess = new Zend_Session_Namespace(PS_App_Auth);
		if(isset($objSess->app_lang) && $objSess->app_lang != ''){	
			if($objSess->app_lang == 'spanish'){
				$objTranslate->setLocale('es');
			} else {
				$objTranslate->setLocale('en');
			}
		}
		
		
        if ($moduleName == 'admin') {
            if($controllerName == "index") {

                if($actionName == "index") {
                    $this->isUserAlreadyLoggedIn();
                }
            } else {
               // $layout->setLayout('admin');
            }
            
            if( Zend_Auth::getInstance()->hasIdentity() ){
            	$objSess = new Zend_Session_Namespace(PS_App_Auth);
            	$this->objSess = $objSess;
            	$this->view->objSess = $this->objSess;
            }

            if(($controllerName != "index")) {
                $this->doAdminAuthorisation();
            }
            
        }
        $layout->setLayout('admin');
        parent::init();
		unset($objGeneralModel,$objGeneralSelect);
    }

    function doAdminAuthorisation() {
        $objSess = new Zend_Session_Namespace(PS_App_Auth);
		if(!isset($objSess->isAdminLogin) || $objSess->isAdminLogin != true)
        {
            $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
            $objError = new Zend_Session_Namespace(PS_App_Error);
            $objError->message = formatErrorMessage($objTranslate->_('LOGIN_REQUIRED'));
            $this->_redirect("/admin/");
        }

    }

    function isUserAlreadyLoggedIn() {
        $objSess = new Zend_Session_Namespace(PS_App_Auth);
		
        if(isset($objSess->isAdminLogin) && $objSess->isAdminLogin == true)
        {
            $this->_redirect("/admin/welcome");
        }
    }

    /**
    * Create thumbnail of an image
    *
    * @param string $sourcePath Path of source image
    * @param string $destPath Destination path of thumbnail
    * @param number $w Width of thumbnail
    * @param number $h Height of thumbnail
    * @param number $q (optional) thumbnail image quality
    */
    protected function _createThumbnail($sourcePath, $destPath, $w, $h, $q = 100)
    {
        $thumb = new PS_Thumbnail($sourcePath);
        $thumb->resize($w, $h);
        $thumb->save($destPath, $q);
        unset($thumb);
    }
	
	protected function uploadfile($Path)
	{
		$upload = new PS_File_Transfer_Adapter_Http();
		$upload->setDestination($Path);
		try {
			$updFlg = $upload->receive();
			return "success";
		} catch (Zend_File_Transfer_Exception $e) {
			return $e->getMessage();
		}
	}
	
	protected function getExtraVar($key=null) {
		return (($key==null) ? $this->_extraVar : ((isset($this->_extraVar[$key])) ? $this->_extraVar[$key] : null));
	}
	
	protected function getSiteVar($key=null) {
		return (($key==null) ? $this->_siteVar : ((isset($this->_siteVar[$key])) ? $this->_siteVar[$key] : null));
	}
	
    function generateCaptcha(){
		$objCaptcha = new Zend_Captcha_Image();
		$objCaptcha
			->setWidth(150)
//			->setHeight(50)
			->setFontSize(30)
			->setFont(FONT_ROOT_PATH.'arial.ttf')
			->setImgUrl(CAPTCHA_PATH)
			->setImgDir(CAPTCHA_ROOT_PATH)
			->setWordLen(4);
		$objCaptcha->generate();
		return $objCaptcha;
	

    }


protected function getFile($FieldName,$DestPath,$DestFileNamePrefix='',$Validators=array(),$newFileName='') {
	$upload = new PS_File_Transfer_Adapter_Http();
  
  if($newFileName != ''){
		$DestFileName=$DestFileNamePrefix.$newFileName; 
  }else{
		$DestFileName=$DestFileNamePrefix.basename($upload->getFileName($FieldName)); 
  }  

  $upload->setFileName($FieldName,$DestFileName);
  $upload->setDestination($DestPath);
  
  if(count($Validators)>0) {
   $upload->setValidators($Validators);
  }

  $Return=array();

  try {

   if ($upload->isValid($FieldName)) {
    $updFlg = $upload->receive($FieldName);
    $Msg = "success";

   } else {
    $Msg= $upload->getMessages();
   }
  } catch (Zend_File_Transfer_Exception $e) {
   $Msg = $e->getMessage();
  }

  if($Msg == "success") {
   $Return[0] = "success";
   $Return[1] = basename($upload->getFileName($FieldName));
   
  } else {
    if(count($Msg)==1 && array_key_exists('fileUploadErrorNoFile',$Msg)) {
    $Return[0] = "nofile";
    $Return[1] = $Msg;
   } else {
    $Return[0] = "failure";
    $Return[1] = $Msg;
   }
  }
  
  
  unset($upload);
  
  return $Return;
 }
 
 /*---------------------------------|Remove faunction|--------------------------------*/	
 
		
		public function remove_directory($directory, $empty=FALSE)
		{
			if(substr($directory,-1) == '/')
			{
				$directory = substr($directory,0,-1);
			}
			if(!file_exists($directory) || !is_dir($directory))
			{
				return FALSE;
			}elseif(is_readable($directory))
			{
				$handle = opendir($directory);
				while (FALSE !== ($item = readdir($handle)))
				{
					if($item != '.' && $item != '..')
					{
						$path = $directory.'/'.$item;
						if(is_dir($path)) 
						{
							remove_directory($path);
						}else{
							unlink($path);
						}
					}
				}
				closedir($handle);
				if($empty == FALSE)
				{
					if(!rmdir($directory))
					{
						return FALSE;
					}
				}
			}
			return TRUE;
		}
		 
}
