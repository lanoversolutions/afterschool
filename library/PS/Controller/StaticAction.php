<?php

class PS_Controller_StaticAction extends Zend_Controller_Action {
	
	private $_extraVar = array();
	private $_siteVar = array();
	
    function init() {
		$objGeneralModel = new Models_GeneralSetting;
        $objGeneralSelect = $objGeneralModel->getGeneralSettingRec();

		foreach($objGeneralSelect as $GeneralSettings) {
			$this->_siteVar[$GeneralSettings['settings_varname']] = $GeneralSettings['settings_value'];
		}
		$this->view->siteTitle = self::getSiteVar('SITE_TITLE');

        $layout = Zend_Layout::getMvcInstance();
        $moduleName     = $this->getRequest()->getModuleName();
        $actionName     = $this->getRequest()->getActionName();
        $controllerName = $this->getRequest()->getControllerName();        
		$this->_extraVar = array('controller'=>$controllerName,'action'=>$actionName,'module'=>$moduleName);
		
		$this->view->frontHeader = "frontHeader.phtml";
		$this->view->frontLeft = "frontLeft.phtml";
		$this->view->frontFooter = "frontFooter.phtml";
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
		
		$layout->setLayout('frontlayout');
		
		parent::init();
        unset($objGeneralModel,$objGeneralSelect,$arrUserData,$objUserModel);
    }

    function doAdminAuthorisation() {
        $objSess = new Zend_Session_Namespace(PS_App_Auth);

        if(!isset($objSess->isAdminLogin) || $objSess->isAdminLogin != true)
        {
            $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
            $objError = new Zend_Session_Namespace(PS_App_Error);
            $objError->message = formatErrorMessage($objTranslate->_('LOGIN_REQUIRED'));
            $this->_redirect("/admin/");
        }

    }

    function isUserAlreadyLoggedIn() {
        $objSess = new Zend_Session_Namespace(PS_App_Auth);

        if(isset($objSess->isAdminLogin) && $objSess->isAdminLogin == true)
        {
            $this->_redirect("/admin/welcome");
        }
    }

    /**
    * Create thumbnail of an image
    *
    * @param string $sourcePath Path of source image
    * @param string $destPath Destination path of thumbnail
    * @param number $w Width of thumbnail
    * @param number $h Height of thumbnail
    * @param number $q (optional) thumbnail image quality
    */
    protected function _createThumbnail($sourcePath, $destPath, $w, $h, $q = 100)
    {
        $thumb = new PS_Thumbnail($sourcePath);
        $thumb->resize($w, $h);
        $thumb->save($destPath, $q);
        unset($thumb);
    }
	
	protected function uploadfile($Path)
	{
		$upload = new PS_File_Transfer_Adapter_Http();
		$upload->setDestination($Path);
		try {
			$updFlg = $upload->receive();
			return "success";
		} catch (Zend_File_Transfer_Exception $e) {
			return $e->getMessage();
		}
	}
	
	protected function getExtraVar($key=null) {
		return (($key==null) ? $this->_extraVar : ((isset($this->_extraVar[$key])) ? $this->_extraVar[$key] : null));
	}
	
	protected function getSiteVar($key=null) {
		return (($key==null) ? $this->_siteVar : ((isset($this->_siteVar[$key])) ? $this->_siteVar[$key] : null));
	}
	
    function generateCaptcha(){
		$objCaptcha = new Zend_Captcha_Image();
		$objCaptcha
			->setWidth(150)
//			->setHeight(50)
			->setFontSize(30)
			->setFont(FONT_ROOT_PATH.'arial.ttf')
			->setImgUrl(CAPTCHA_PATH)
			->setImgDir(CAPTCHA_ROOT_PATH)
			->setWordLen(4);
		$objCaptcha->generate();
		return $objCaptcha;
    	
    }
}
