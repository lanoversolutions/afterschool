<?php
class Zend_View_Helper_FormatPhoneNumber {
	function formatPhoneNumber($phone_number, $phone_ext) {
		$return_phone = $phone_number;
		$stripped_phone_number = preg_replace("/[^0-9]/", "", $phone_number);
		if(strlen($stripped_phone_number) == 7)
			$return_phone = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $stripped_phone_number);
		elseif(strlen($stripped_phone_number) == 10)
			$return_phone = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $stripped_phone_number);

		if ($phone_ext <> "")
			return $return_phone .", ext " . $phone_ext;
		else
			return $return_phone;
	}
}