<?php
class Zend_View_Helper_FormatURL {
	function formatURL($url) {
		if(preg_match( "/[http|https]:/i",$url)) {
			return "<a href=\"$url\" rel=\"nofollow\" target=\"blank\">$url</a>";			
		} else if(preg_match( "/[@|\s]/i",$url)) {
			return $url;			
		}else {
			return "<a href=\"http://$url\" rel=\"nofollow\" target=\"blank\">$url</a>";
		}
	}
}