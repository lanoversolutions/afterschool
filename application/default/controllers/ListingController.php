<?php


/**
 * ListingController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */

class ListingController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
	
		//Google Rightsideadsense
		$rightside = $this->view->partial('rightside.phtml' ,array());
		$this->view->rightside = $rightside;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;

	}



	/**
	 * The "state" action is use to display a statelisting
	 *
	 * This action to use the display state listing.
	
	 /* state Action Start*/

	public function stateAction(){			
		
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		
		$objModel = new Models_States ();
		
		$statename = $objRequest->statename;
		$state = array ();
		$state = $objModel->fetchstatename( $statename );

		$state_code = $state['state_code'];		
		
		$objCityModel = new Models_Cities();
		$cities = array ();
		$cities = $objCityModel->fetchcity( $state_code );

		$this->view->siteTitle = $state['state_name'] . " After School Programs | " . $state['state_name'] . " Summer Camps and After School Care | After School Programs";
		$this->view->state = $state;
		$this->view->cities = $cities;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->currentAction = 'state';
		unset ($objModel, $objRequest, $objTranslate );
					
	}

	
		/**
	 * The "displayDetail" action is use to display a details
	 *
	 * This action to use the display details.
	
	 /* displayDetail Action Start*/

	
	public function detailAction(){			
		
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		
		$objModel = new Models_Afterschool ();
		
		$id = $objRequest->id;
		$user_id = $objSess->user_id;
		
		global $arrRating_Type;
		
		$objReview = new Models_Reviews ();	
		
		
		//Code Added by Vijay is used in login restriction [START]
		$objTempSess = new Zend_Session_Namespace('temp_session');
		
		if(isset($objTempSess->tempsession_data) && !empty($objTempSess->tempsession_data)){
			
			if ($objTempSess->tempsession_data['table'] =="activity") {
				 $objSModel = new Models_Activity();
			} else {
				$objSModel = new Models_Afterschool ();
			}
			
			$objReviewModel = new Models_Reviews ();
			
			$objTempSess->tempsession_data['user_id'] = $objSess->user_id;
						
			$objReviewModel->saveData ( $objTempSess->tempsession_data );

			$this->view->message = "Thank you for your comment. <br/><br/>";
		  		$this->view->message .= "<br/> <a href=".$this->_helper->url->url(array('controller'=>'listing','action'=>'detail','module'=>'default','id'=> $objTempSess->tempsession_data['provider_id'],'filename'=> $objTempSess->tempsession_data['provider_filename']),'listingdetails','escape',false, true).">Return to listing details</a>";
			
			$where = array('id = ?' => $objTempSess->tempsession_data['provider_id']);
			$provider = $objSModel->fetchRow($where);
			$this->view->provider = $provider;
			
			
			unset($objTempSess->tempsession_data);
			$this->renderScript('review/new.phtml');
		}
		
		
		//Code Added by Vijay [END]
			
		$program = array ();
		$program = $objModel->fetchdata( $id );
		
		$statecode = $program['state'];	
		$cityname =  $program['city'];
		
		$objStatesModel = new Models_States();
		$state = array ();
		$state = $objStatesModel->fetchstatelisting( $statecode );
		
		$objCitiesModel = new Models_Cities();
		$city = array ();
		$city = $objCitiesModel->fetchcitylisting( $cityname,$statecode );

		$objImagesModel = new Models_Images();
		$images = array ();
		$images = $objImagesModel->fetchMartialArtsImages($id);


		$reviews = array ();
		$reviews = $objReview->getApprovedReviewList($id);

		$objForm = new Models_Form_Review($id);
		
		if ($objRequest->isPost ()) {
		
		$formData = $objRequest->getPost ();
							
			if ($objForm->isValid ( $formData )) {	
			
			$this->_forward("new");
			
			}else{
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}

		$this->view->siteTitle = $state['state_name'] . " After School Programs | " . $state['state_name'] . " Summer Camps and After School Care | After School Programs";



		$this->view->reviews = $reviews;
		$this->view->arrRating_Type = $arrRating_Type;
		$this->view->images = $images;
		$this->view->program = $program;
		$this->view->state = $state;
		$this->view->city = $city;
		$this->view->user_id = $user_id;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->objForm = $objForm;
		$this->view->currentAction = 'detail';
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );					
	}
	
	
	
	
		/**
	 * The "city" action is use to display a City Listing.
	 *
	 * This action to use the display City Listing.
	 
	 /* city Action Start*/

	public function cityAction(){			
		
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		
		
		$objModel = new Models_Cities ();
		
		$filename = $objRequest->filename;

		$city = array ();
		$city = $objModel->fetchcityname( $filename );

		if($city == null) {
			$this->_redirect("/");
		}
		
		$statename = $city['statefile'];	
		
		$objModelStates = new Models_States ();
		
		$state = array ();
		$state = $objModelStates->fetchstatename($statename);
		
		$state_code = $state['state_code'];
		$cityname	= $city['city'];
		
		$objModelafterschool = new Models_Afterschool ();
		$facilities = $objModelafterschool->fetchafterschoolData($state_code,$cityname);
		
		$this->view->siteTitle = ucwords(strtolower($city['city'])) . ", " . $city['state'] . " After School Care | Afterschool Programs in " . ucwords(strtolower($city['city'])) . " " . $city['state']."| After School Programs";
		
		$this->view->facilities = $facilities;
		$this->view->state = $state;
		$this->view->cities = $city;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->currentAction = 'city';
		unset ($objModel, $objRequest, $objTranslate );
					
	}

	

		/**
		 * The "new" action is use to save review data
		 *
		 * This action to use the save review data.
		 
		 /* new Action Start*/


	public function newAction() {
					   
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = "Create Your Own Review | After School Programs";
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
				
		$id = $objRequest->id;
		
		if(strlen($id) < 2){
			$this->_redirect("/");
		}
				
		$type = "activity";
		
		$RequestType = $objRequest->type;
		
		if ($RequestType =="activity") {
			 $objModel = new Models_Activity();
		} else {
			$objModel = new Models_Afterschool ();
			$type = "program";
		}
		
		
		$where = array('id = ?' => $id);
		$provider = $objModel->fetchRow($where);		
				
		if ($provider == null) {
			$this->_redirect("/");
		}
		
		$this->view->provider = $provider;
		
		$objModel = new Models_Reviews ();
		$objForm = new Models_Form_Review ($id,$RequestType);	
		
	    				
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			
			
			 if($objSess->user_id == ''){
			 	$objTempSess = new Zend_Session_Namespace('temp_session');
				$objTempSess->alive = 1;
				$objTempSess->reviewFormData = $formData;
				
				$tempSessionData = array();
				
				$tempSessionData['email'] = $formData['email'];
				$tempSessionData['name'] = $formData['name'];
				$tempSessionData['rating'] = $formData['rating'];
				$tempSessionData['comments'] = $formData['comments'];
				//$tempSessionData['id'] = $formData['id'] ;
				$tempSessionData['type'] = $formData['type'];
				
				$tempSessionData['provider_id'] = $id;
				$tempSessionData['ip_address'] = $_SERVER['REMOTE_ADDR'];
				$tempSessionData['provider_name'] = $provider['name'] ;
				$tempSessionData['review_by'] = $formData['name'] ;
				$tempSessionData['provider_filename'] = $provider['filename'] ;
				$tempSessionData['user_id'] = $objSess->user_id;
				$tempSessionData['table'] = $RequestType ;
				
				$objTempSess->tempsession_data = $tempSessionData;
				
				unset($tempSessionData);
				
				$objError->message = $objTranslate->translate('FRONT_MSG_INVALID_LOGIN');
				$objError->messageType = 'error';	
				$objTempSess->request_url = SERVER_HTTP.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
				$this->_redirect ( "/user/login" );
			}
			
							
			if ($objForm->isValid ( $formData )) {
				
				$formData['provider_id'] = $id;
				$formData['ip_address'] = $_SERVER['REMOTE_ADDR'];
				$formData['provider_name'] = $provider['name'] ;
				$formData['review_by'] = $formData['name'] ;
				$formData['provider_filename'] = $provider['filename'] ;
				$formData['user_id'] = $objSess->user_id ;
				$formData['table'] = $RequestType ;
				
				
			    $retrivedId = $objModel->saveData ( $formData );
			
				$this->view->message = "Thank you for your comment. <br/><br/>";
/*		  		$this->view->message .= "A confirmation has been sent to " . $formData['email'] . ". Please check your email and click on the confirmation link before the review is published.<br/><br/>";
		  		$this->view->message .= "If your email address is not " . $formData['email'] . " or if you think you have made an error, please hit the back button on your browser to correct and resubmit your form. <br/>";
*/		  		$this->view->message .= "<br/> <a href=".$this->_helper->url->url(array('controller'=>'listing','action'=>'detail','module'=>'default','id'=> $provider['id'],'filename'=> $provider['filename']),'listingdetails','escape',false, true).">Return to listing details</a>";
				

				$arrData = array();
				$arrData['provider']= $provider;
				$this->view->arrData = $arrData;
				$this->renderScript('review/new.phtml');
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
		
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Add';
		$this->view->objForm = $objForm;
	}
	/*Add new Action End*/





	/**
	 * The "confirmEmail" action is use to Send email
	 *
	 * This action to use the Send email.
	 
	 /* confirmEmail Action Start*/

	public function confirmEmail($formData) {
		
	    //Create Zend_Mail object.
	    $MailObj = new Zend_Mail();
	    $bodyText = "Hello, \n\n";
	    $bodyText .= "We received the following review for " . $formData['provider_name'] . ":<br/> ";
	    $bodyText .= "---------- <br/> ";
	    $bodyText .= $formData['comments'];
	    $bodyText .= "---------- <br/><br/>";
	    $bodyText .= "To verify that you are the person submitted the review above, please follow this link:<br/><br/>";  
	    $bodyText .= "http://lanover.com/lan/afterschool/public/review/verify?id=" . $formData['id'] . "&e=" . $formData['email'] . "<br/><br/>";
	    //$bodyText .= "http://afterschoolprograms.us/review/verify?id=" . $formData['id'] . "&e=" . $formData['email'] . "<br/><br/>";
		$bodyText .= "If you are unable to click on the link above, copy and paste the full link into your web browser's address bar and press enter. <br/><br/>";
	    $bodyText .= "Thanks sincerely";
	    $MailObj->setBodyHtml($bodyText);
	    $MailObj->setFrom("support@afterschoolprograms.us","Do Not Reply");
	    $MailObj->addTo($formData['email'], $formData['review_by']);
	    $MailObj->setSubject("Your Review for " . $formData['provider_name']);
	    //Send Email using transport protocol.
	    try{
	    	//$MailObj->send($this->getTransport());
			$MailObj->send();
	    }catch(Zend_Mail_Exception $e){
	        $this->view->message = $e->getMessage();
	    }
    }
	

    public function getTransport() {
    	//Create SMTP connection Object		
    	$configInfo = array('auth' => 'login',
                   'username' => 'support@afterschoolprograms.us',
                   'password' => 'conheo#1');
   		$smtpHost = new Zend_Mail_Transport_Smtp('mail.afterschoolprograms.us',
                                 $configInfo);
        return $smtpHost;
    }


	/**
	 * The "verify" action is use to verify Your review
	 *
	 * This action to use the verify Your review.
	 
	 /* verify Action Start*/

	public function verifyAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		//$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$this->view->siteTitle = "Verify Email Address | After School Programs";
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$id = $objRequest->id;
		$email = $objRequest->e;
		
		if (strlen($id) == 0 && strlen($email) == 0) {
			$this->_redirect("/");
		}else {
		
			$objModel = new Models_Reviews ();
			$review = array ();
			$review = $objModel->fetchReviewsData($id,$email);

			$provider_id = $review['provider_id'];
			
			if ($review != null) {
				$review['email_verified'] = 1;
				
				$objModel->updateData ($review , $id ,$provider_id);
				$type = "activity";
				
			if ($review['table']=="activity") {
					$objproviderModel = new Models_Activity();
				} else {
					$objproviderModel = new Models_Afterschool ();
					$type = "program";
				}
				$where = array('id = ?' => $review['provider_id']);
				$provider = $objproviderModel->fetchRow($where);

				$this->view->provider = $provider;
				$this->view->message = "Thank you for verify your email address.  Your comment will be reviewed for approval within 1-2 business days. ";
		  		$this->view->message .= "<br/> <a href=".$this->_helper->url->url(array('controller'=>'listing','action'=>'detail','module'=>'default','id'=> $provider['id'],'filename'=> $provider['filename']),'listingdetails', true).">Return to listing details</a>";
				
				$arrData = array();
				$arrData['provider']= $provider;
				$this->view->arrData = $arrData;
				$this->renderScript('review/verify.phtml');			
					
		}else {
			$this->_redirect("/");
			}
		}
		
		
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->currentAction = 'verify';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
 
}
