<?php

/**
 * UserController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class UserController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
		$fbbutton_data = $this->view->partial('fbbutton.phtml' ,array());
		
		$this->view->fbbutton_data = $fbbutton_data;

		//Google Rightsideadsense
		$rightside = $this->view->partial('rightside.phtml' ,array());
		$this->view->rightside = $rightside;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;
	
	}
	
	
    public function indexAction() 
    {
		$objRequest = $this->getRequest ();
		$this->view->siteTitle = "Welcome";
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );								
		
												
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';		    	
    }	




	/**
	 * The "edit" action is use to edit a Account
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/account
	 *
	 * @return void
	 */
	/*Edit Accountedit Action Start*/
	public function accountAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_ACCOUNT_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);

		$objModel = new Models_Frontuser();
		$objForm = new Models_Form_Frontuser();					
		
		$objForm->Account();
		
		$id = $objSess->user_id;										
		$arrData = array ();
		$arrData = $objModel->fetchaccountEntry ( $id );																
			
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();			
		
			if ($objForm->isValid ( $formData )) {
					
					//_pr($arrData,1);
					$arrData['user_option'] = $formData['user_option'];
																		
					$objModel->updateaccountData ($arrData,$id);					
					
					//update the session for type
					$arrUserData = $objModel->fetchaccountEntry ($id);
					$objSess->user_option = $arrUserData['user_option'];
					
					$objError->message = $objTranslate->translate('FRONT_MSG_VALID_ACCOUNT_INSERT');
					$objError->messageType = 'confirm';
				
					
				$objTempSess = new Zend_Session_Namespace('temp_session');	
				
				if(isset($objTempSess->request_url) && !empty($objTempSess->request_url)){
							$tempUrl=$objTempSess->request_url;
							unset($objTempSess->request_url);
							$this->_redirect($tempUrl);
				}else {	
						
					if($formData['user_option']== '1'){
							$this->_redirect ( "/parent/dashboard" );
					}else{
							$this->_redirect ( "/provider/dashboard" );
					}
				}
							
			} else {
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';

			}
						//Auto populate the records
			$objForm->populate ( $arrData );
			
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'account';
		$this->view->objForm = $objForm;
		$this->view->id = $id;	
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );			
	}
	/*Edit Pagesedit Action End*/
	


	
	
		/**
	 * The "register" action is use to add a user
	 *
	 * This action to use the add data
	 
	 * via the following urls:
	 *
	 * /user/register
	 *
	 * @return void
	 */
	/*Add register  Action Start*/
	
		public function registerAction() {
			   
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Register ();
		$objForm = new Models_Form_Register ();	
		
	    				
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
							
			if ($objForm->isValid ( $formData )) {
				
				
				//Check existing record
				$flag = $objModel->checkRedudancy($formData);
								
				if($flag == 1){
					 $objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NOT_MATCH');	
					 $objError->messageType = 'error';
				}else if($flag == 2){
					$objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_REGISTRATIONUSER_ALLREADY_TAKEN');
					$objError->messageType = 'error';	
				}else if($flag == 3){
					$objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NOT_MATCH');
					$objError->messageType = 'error';		
				}else{
					
					//create uniqid
					$formData['confirm_key'] = uniqid();
					
					// Save data
				    $retrivedId = $objModel->saveData ( $formData );
					
					if ($retrivedId){
						
							$html = new Zend_View();
							$html->setScriptPath(EMAIL_BODY_PATH);
							
						/* Send activation link to register user in email*/ 
							$from = CONFIGURATION_NAME_FROM.'<'.CONFIGURATION_EMAIL_FROM.'>';
							$to =  $formData['email'];
							$mail_subject='Thanks for Choosing After School Programs: Email comfirmation';
							
							$mail_body = $html->render('email.phtml');
							$mail_body = str_replace('{$confirmation_url}',ACTIVATION_USER_CODE_PATH.$formData['confirm_key'],$mail_body);
							$mail_body = str_replace('{$confirmation_email}',$formData['email'],$mail_body);
							$mail_body = stripslashes($mail_body);
							
							$headers = "MIME-Version: 1.0\r\n";
							$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							$headers .= "From: ".$from."\r\n";
							
							$objSendMail =new Models_SendEmail();
							$objSendMail->sendEmail($to, $mail_subject, $mail_body, $headers);
									
						/* End Activation link*/
					}
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_REGISTRATIONUSER_INSERT');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/user/login" );
					
				}
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Add';
		$this->view->objForm = $objForm;
	}
	/*Add pagesadd Action End*/
		


	/**
	 * The "userprofile" action is use to edit a userprofile
	 *
	 * This action to use the edit data 
	 * via the following urls:
	 *
	 * /user/userprofile
	 *
	 * @return void
	 */
	/*Edit userprofile Action Start*/
	public function userprofileAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$objModel = new Models_Frontuser ();
		$objForm = new Models_Form_Register ();
		//create the userprofile form object.
		$objForm->Userprofile();
		
		$id = $objSess->user_id;									
		$arrData = array ();
		$arrData = $objModel->fetchuserdetails ($id , $objSess->user_id );																
	  
	  
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
									
			if ($objForm->isValid ( $formData )) {	
				
				// password convert to md5
				$password = md5($formData['password']); 
				
				// fetch password in database
				$arrpassword = $objModel->fetchpassword($password);
				
				if($arrpassword){
				
				  if($formData['newpassword'] != $formData['retype_password']){
				
					$objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_PASSWORD_NOT_MATCH');	
					$objError->messageType = 'error';
				
				  }else{
					//new password convert to md5 									
					$formData['password'] = md5($formData['newpassword']);
															
					$objModel->updateData ( $formData , $id);
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_REGISTRATIONUSER_UPDATE');
					$objError->messageType = 'confirm';	
					$this->_redirect ( "/user/userprofile");											
				 }
				 
			}else{   
					$objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_PASSWORD');	
					$objError->messageType = 'error';
			   }
			}else {	
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}else{
			//Auto populate the records
			$objForm->populate ( $arrData );
		} 
					
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->objForm = $objForm;
		$this->view->id = $id;
		$this->view->currentAction = 'userprofile';
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );
	}
	/*Edit Action End*/
		
		
		
	/**
	 * The "login" action is use to login user
	 *
	 * This action to use the edit data 
	 * via the following urls:
	 *
	 * /user/login
	 *
	 * @return void
	 */
	/*login Action Start*/
	 public function loginAction()
    {    	
    	
    	$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
    	$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
    	$objError = new Zend_Session_Namespace(PS_App_Error);
		$this->view->siteTitle = $objTranslate->translate('USER_LABEL_PAGETITLE_LOIGN');

		$objRequest = $this->getRequest();
		$objModel = new Models_UserLogin() ;
		$objForm = new Models_Form_UserLogin();
					
        if ($objRequest->isPost()) {
            $formData = $objRequest->getPost();
			
            if ($objForm->isValid($formData)){
				if(isset($formData['submit'])) {
                    $isValid = $objModel->verifyLoginInfo($formData);
					                    
					if ($isValid) {
                    	$objModel->setSession();
                    	
						$objTempSess = new Zend_Session_Namespace('temp_session');
						if(isset($objTempSess->request_url) && !empty($objTempSess->request_url)){
							$tempUrl=$objTempSess->request_url;
							unset($objTempSess->request_url);
							$this->_redirect($tempUrl);
						}
						//_pr($objSess,1);
						
						if($objSess->user_option == '0'){
							
							$objError->messageType = 'confirm';
							$this->_redirect("/user/account");
							
						}elseif($objSess->user_option == '1' ){
							$objError->messageType = 'confirm';
							$this->_redirect("/parent/dashboard");
						  }else{
							$objError->messageType = 'confirm';  
							$this->_redirect("/provider/dashboard");
						}
						
				    } else {
	                    $objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_LOGIN');
	                    $objError->messageType = 'error';
                        $this->_redirect("/user/login");
                    }
                }
            }
            else
            {
                $objForm->populate($formData);
                $objError->message = formatErrorMessage($objForm->getMessages());                
                $objError->messageType = 'error';
            }
           
        }
		
        $this->view->message = $objError->message;
        $this->view->messageType = $objError->messageType;
        $objError->message  = "";
		$objError->messageType = "";
		
        $this->view->objForm = $objForm;
        $this->view->leftPanelFlag	= '';
        //$this->_helper->layout->setLayout('userlogin');
       }
	   /*Login Action End*/
    
	
	

 /*----------------| Forgot password Action |---------------------*/    
    public function forgotpassAction()
    
	{
      $objTranslate = Zend_Registry::get('Zend_Translate'); 
      $objRequest = $this->getRequest(); 
      $this->view->siteTitle = "Forgot Password";
      $objError = new Zend_Session_Namespace ( PS_App_Error );
	  $objSess = new Zend_Session_Namespace(PS_Front_App_Auth);	
      $objModel = new Models_UserLogin();		
	  $objForm =new Models_Form_ForgotPassword();
	  if ($objRequest->isPost()) 
		{
		     $formData = $objRequest->getPost();

	    	 if ($objForm->isValid($formData)) 
			 {
					$user_detail=$objModel->checkemail(trim($formData['user_email']));
					//echo "<pre>";print_r($user_detail);exit;
					if($user_detail)
					{
				//--------------Send Email Section--------------//
					$html = new Zend_View();
					$html->setScriptPath(EMAIL_BODY_PATH);
					
				/* Send activation link to register user in email*/ 
					$from = CONFIGURATION_NAME_FROM.'<'.CONFIGURATION_EMAIL_FROM.'>';
					$to =  $user_detail['email'];
					$mail_subject='Forgot Your Password? : Password comfirmation';
					
					$mail_body = $html->render('forgotpassemail.phtml');
					$mail_body = str_replace('{$confirmation_url}',CHECK_FORGOTPASS_PATH.$user_detail['confirm_key'],$mail_body);
					$mail_body = str_replace('{$confirmation_email}',$user_detail['email'],$mail_body);
					$mail_body = stripslashes($mail_body);
					
					$headers = "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$headers .= "From: ".$from."\r\n";
					
					$objSendMail =new Models_SendEmail();
					$objSendMail->sendEmail($to, $mail_subject, $mail_body, $headers);

					
/*					//$mail = new Zend_Mail();
					$from = CONFIGURATION_EMAIL_FROM;
					$to =  $user_detail['email'];
					$mail_subject='HuyVo Property : New password';
					//$mail_body =  'Username : '.$user_detail['email'].'<br>'.'Password : '.$newPass;
					$mail_body = CHECK_FORGOTPASS_PATH.$user_detail['confirm_key'];
					
					$headers = "From: ".$from."\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					
					//echo $mail_body; exit;
					$objSendMail = new Models_SendEmail();
					$objSendMail->sendEmail($to, $mail_subject, $mail_body, $headers);
*/					
					$objError->message = $objTranslate->translate("FORGOTPASS_MAIL_SEND");
					$objError->messageType = 'confirm';				
            		$this->_redirect("/user/login");
												
				}else{
					$objError->message = $objTranslate->translate("EMAIL_ADDRESS_NOT_EXIST");
					$objError->messageType = 'error';
			}
			 }
			 else{
                $objForm->populate($formData);
                $objError->message = formatErrorMessage($objForm->getMessages());
				$objError->messageType = 'error';
			}
		}

		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
	    $this->view->objForm = $objForm;
	    $this->view->leftPanelFlag	= '';
    }

	/**
	 * The "activation" action is use to update the user_status
	 *
	 * This action to use the edit data 
	 * via the following urls:
	 *
	 * /user/register
	 *
	 * @return void
	 */
	/*activation Action Start*/
	
	public function activationAction() {	
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_ACTIVATIONACCOUNT_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Register ();
		//fetch the conformation key
		$confirm_key = $objRequest->confirm_key;										
		$arrData = array ();
		$arrData = $objModel->fetchkey ($confirm_key);
		
		if(!empty($arrData)){

			$formData['user_status']='1';
			
			$objModel->updateData ( $formData, $arrData['user_id'] );
			
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_ACTIVATIONACCOUNT_UPDATE');
			$objError->messageType = 'confirm';			
			$this->_redirect ( "/user/login" );
		} else {
			$objError->message = formatErrorMessage ( $objForm->getMessages () );
			$objError->messageType = 'error';
			$this->_redirect ( "/user/login" );
		}
				
		//exit;		
	}
	
	
	/*activation Action End*/
	
	
		public function checkpassAction() {	
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_CHECKPASS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Register ();
		//fetch the conformation key
		
		$confirm_key = $objRequest->confirm_key;										
		$arrData = array ();
		$arrData = $objModel->fetchkey ($confirm_key);
		
		if(!empty($arrData)){
				
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_NEWPASS');
			$objError->messageType = 'confirm';			
			$this->_redirect ( "/user/resetpassword/user_id/".$arrData['user_id'] );
		} else {
			$objError->message = formatErrorMessage ( $objForm->getMessages () );
			$objError->messageType = 'error';
			$this->_redirect ( "/user/login" );
		}
				
		//exit;		
	}


		/**
	 * The "edit" action is use to edit a  tenantaccount user
	 *
	 * This action to use the edit data 
	 * via the following urls:
	 *
	 * /user/tenantregister
	 *
	 * @return void
	 */
	/*Edit tenantaccount Action Start*/
	public function resetpasswordAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_RESETPASSWORD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		 $objModel = new Models_UserLogin();		
		 $objForm =new Models_Form_ForgotPassword();
		
		$objForm->Resetpassword();	
		
		$user_id = $objRequest->user_id;										
		$arrData = array ();
		$arrData = $objModel->fetchuserid ($user_id);																
	  		  
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
									
			if ($objForm->isValid ( $formData )) {	
			
				//_pr($formData,1);		
				$objModel->SetNewPasswrod ( $formData,$arrData['user_id']);
				
				$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_PASSWORD_RESET');
				$objError->messageType = 'confirm';	
				$this->_redirect ( "/user/login");										
			}else {
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}			
		}
							
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->objForm = $objForm;
		$this->view->id = $user_id;
		$this->view->currentAction = 'resetpassword';
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );
	}
	/*Edit Action End*/	
		
	 /* The "Logout" action is logout the login user*/
	/*Logout Action Start*/
	
	 /*----------------| logout Action |---------------------*/    
    public function logoutAction()
    {	
		$objRequest = $this->getRequest();
        $objModel = new Models_UserLogin();						
        $objModel->clearSession();
		
		if(isset($_SESSION['temp_form_session'])){
			unset($_SESSION['temp_form_session']);
		}
		
		unset($_SESSION['PS_Auth']);
		//_pr($_SESSION,1);
		
		$this->_redirect("/");
    }
	
	
	 /* The "facebook login" action is login to application using facebook */
	
	/*facebook login Action Start*/

	
	/*----------------| facebook login Action |---------------------*/    
    public function fbloginAction()
    {	    	
    	$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
    	$objError = new Zend_Session_Namespace(PS_App_Error);
    	$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objRequest = $this->getRequest();
		$objUserLogin = new Models_UserLogin();
		$objRegister = new Models_Register ();
    	//include facebook class
		require 'Facebook/src/facebook.php';
    	$full_url = $objRequest->getScheme().'://'.$objRequest->getHttpHost().$objRequest->getRequestUri();
		
    	$fbconfig = Zend_Registry::get('fbconfig');		 			
    	$facebook = new Facebook(array(
			'appId' => $fbconfig->app_id,
			'secret' => $fbconfig->secret_key,			
			'cookie' => true		
		));
		
    	$user = $facebook->getUser();
    	// Login or logout url will be needed depending on current user state.
		if ($user) {			
			$access_token = $facebook->getAccessToken();
						
			$logoutUrl = $facebook->getLogoutUrl(
				array( 
				'next' => $full_url,
				'access_token'=>$access_token  
				)
			);
																								        	       		       		           	
       		$user_profile = $facebook->api(
       			array(
	       		'method'=>'fql.query',
	       		'query'=>"SELECT uid,username,first_name,last_name,name,pic_square,profile_url,email FROM user WHERE uid = me()",
	       		'access_token'=>$access_token 
	       		)
	      	);       		       			       	       		
			
	      	//_pr($user_profile ,1);	      		      		      		     	      			
			if( isset($user_profile) ){
				$data = array();
				$data['first_name'] = $user_profile[0]['first_name'];
				$data['last_name'] = $user_profile[0]['last_name'];
				$data['email'] = $user_profile[0]['email'];
				$data['password'] = $user_profile[0]['email'];				
				$data['facebook_identifier'] = $user_profile[0]['uid'];
				$data['user_status'] = 1;
				$data['user_type'] = 'facebookuser';
				$data['confirm_key'] = uniqid();
				//_pr($data,1);
				
				$arrDataemail = $objRegister->fetchemail($data['email']);
				//_pr($arrDataemail,1);
				
				if($arrDataemail){
				
					    $isValid = $objUserLogin->verifyLoginInfo($data);
	
					if ($isValid) {
                	    $objUserLogin->setSession();
						

						$objTempSess = new Zend_Session_Namespace('temp_session');							
						if(isset($objTempSess->request_url) && !empty($objTempSess->request_url)){
							$tempUrl=$objTempSess->request_url;
							unset($objTempSess->request_url);
							$this->_redirect($tempUrl);
						}

						
					if($objSess->user_option == '0'){
						
							$objError->messageType = 'confirm';
							$this->_redirect("/user/account");
						
					}elseif($objSess->user_option == '1' ){
							$objError->messageType = 'confirm';
							$this->_redirect("/parent/dashboard");
						  }else{
							$objError->messageType = 'confirm';  
							$this->_redirect("/provider/dashboard");
					}		
					
					$this->_redirect("/user/account");
										                    
			    } else {
                    $objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_LOGIN');
                    $objError->messageType = 'error';
                    $this->_redirect("/");
                }		
                	
				}
				
				$validate = new Zend_Validate_Db_RecordExists('front_user', 'facebook_identifier');
				if(!$validate->isValid($user_profile[0]['uid'])){															
					$objRegister->saveData ( $data );			
				}				
				//fo	r varify facebook user is valid or not 
				$isValid = $objUserLogin->verifyLoginInfo($data);
				
				
				if ($isValid) {
                	$objUserLogin->setSession();
					
					if($objSess->user_option == '0'){
						
							$objError->messageType = 'confirm';
							$this->_redirect("/user/account");
						
					}elseif($objSess->user_option == '1' ){
							$objError->messageType = 'confirm';
							$this->_redirect("/parent/dashboard");
						  }else{
							$objError->messageType = 'confirm';  
							$this->_redirect("/provider/dashboard");
					}		
					
					$this->_redirect("/user/account");
			    
				} else {
                    $objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_LOGIN');
                    $objError->messageType = 'error';
                    $this->_redirect("/");
                }				
			}		
					
		} else {
			$loginUrl = $facebook->getLoginUrl(
							array(
							'client_id' => $fbconfig->app_id,							
							'scope' => 'email ,publish_stream',
							'redirect_uri' => $full_url,							
							)
						);			
			$this->_redirect ( $loginUrl );
		}				
    	exit;
    }
	
	
	
	/**
	 * The "edit" action is use to edit a  tenantaccount user
	 *
	 * This action to use the edit data 
	 * via the following urls:
	 *
	 * /user/getcounty
	 *
	 * @return county 
	 */	
	public function getcountyAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
				
				
		/* for County combo box */						
		$objModelProperty = new Models_Property ();				
		$arrCounty = $objModelProperty->getCountyCombobox( $objRequest->state_id );																		
		/*County combo box end */
	  				
		echo json_encode($arrCounty);
		exit;
	}
	
		
			
	
	
}
?>
