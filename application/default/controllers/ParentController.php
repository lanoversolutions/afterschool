<?php

/**
 * ParentController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class ParentController extends PS_Controller_FrontAction
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
		//Google Rightsideadsense
		$rightside = $this->view->partial('rightside.phtml' ,array());
		$this->view->rightside = $rightside;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;

	}
	


	/**
	 * The "dashboard" action is use to display a parent lising
	 *
	 * This action to use the display parent lising.
	 
	 * via the following urls:
	 *
	 * /parent/dashboard
	 *
	 * @return void
	 */
	 /* dashboard Action Start*/
	
    public function dashboardAction() 
    {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		 
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );	
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);							
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_INDEX');
		
		$user_option = $objSess->user_option;
		
		if($user_option == NOTSELECTED){
			$this->_redirect ( "/user/account" );	
		}else if($user_option != PARENT){
			$this->_redirect ( "/provider/dashboard" );	
		} 
																
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';		    	
    }
	
		/**
	 * The "review" action is use to display a review lising
	 *
	 * @return void
	 */
	 /* review Action Start*/
	
    public function reviewAction() 
    {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		 
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );	
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);							
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_INDEX');
		

		$objModel = new Models_Reviews ();		
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getreviewsList ( $searchText, $searchType, $sortby , $objSess->user_id );
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		//$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );			    	
    }
	
	
			
	
}
?>
