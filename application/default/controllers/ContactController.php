<?php

/**
 * ContactController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class ContactController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
	
	    //Google Rightsideadsense
		$rightsidetextadsense = $this->view->partial('rightsidetextadsense.phtml' ,array());
		$this->view->rightsidetextadsense = $rightsidetextadsense;
	
	}
	
	
	
	
	/**
	 * The "index" action is use to add a contact
	 *
	 * This action to use the add data
	 
	 * via the following urls:
	 *
	 * /contact/index
	 *
	 * @return void
	 */
	/*index Action Start*/
	
		public function indexAction() {
			   
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_CONTACT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		//$objModel = new Models_Contact ();
		$objForm = new Models_Form_Contact ();	
		
	    				
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
					
			if ($objForm->isValid ( $formData )) {
				 // Save data
				 // $retrivedId = $objModel->saveData ( $formData );
				//_pr($formData,1);
					$configInfo = array('auth' => 'login',
								'username' => 'support@afterschoolprograms.us',
								'password' => 'conheo#1');
					$smtpHost = new Zend_Mail_Transport_Smtp('mail.afterschoolprograms.us',$configInfo);
					//Create Zend_Mail object.
					$MailObj = new Zend_Mail();
					//$to         = "support@afterschoolprograms.us";
					$to         = "pmehul20@gmail.com";
					/*$message	= 'Name:-'.' '.$formData['message'];
					$message	= 'Email:-'.' '.$formData['email'];
					$message	= 'Subject:-'.' '.$formData['subject'];*/
					$message	=  'Name:-'.' '.$formData['name'].'<br/>'.'Email:-'.' '.$formData['email'].'<br/>'.'Subject:-'.' '.$formData['subject'].'<br/>'.'Message:-'.' '.strip_tags($formData['message']);
						
					$MailObj->setBodyHtml($message);
					$MailObj->setFrom($formData['email'],$formData['name']);
					$MailObj->addTo($to);
					$MailObj->setSubject($formData['subject']);
					//Send Email using transport protocol.
					try{
						$MailObj->send($smtpHost);
						$objError->message = "Email sent successfully";
						$objError->messageType = 'confirm';
									
					}catch(Zend_Mail_Exception $e){
						//Your error message here.
						$this->view->message = $e->getMessage();
					}
					//Suppress the view.
					$this->_helper->viewRenderer->setNoRender();									
						/* End Activation link*/
					//$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_CONTACT_INSERT');
					//$objError->messageType = 'confirm';
					
					$this->_redirect ( "/contact/index" );
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'index';
		$this->view->objForm = $objForm;
	}
	/*Add pagesadd Action End*/

	
	
	
	
}
?>
