<?php 

/**
 * ErrorController
 */ 
class ErrorController extends PS_Controller_FrontAction 
{ 
    /**
     * errorAction() is the action that will be called by the "ErrorHandler" 
     * plugin.  When an error/exception has been encountered
     * in a ZF MVC application (assuming the ErrorHandler has not been disabled
     * in your bootstrap) - the Errorhandler will set the next dispatchable 
     * action to come here.  This is the "default" module, "error" controller, 
     * specifically, the "error" action.  These options are configurable, see 
     * @return void
     */
	public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
		
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
				$this->view->message = "No controller is available to service your request.";
				$this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
				$this->view->siteTitle = 'HTTP/1.1 404 Not Found';
				break;
			
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found                
                $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
                $this->view->siteTitle = 'HTTP/1.1 404 Not Found';
                $this->view->message = "Sorry the page you requested has not been found.";
                break;
				
            default:
                // application error; display error page, but don't change                
                // status code
                $this->view->siteTitle = 'Application Error';
                $this->view->message =
                "An unexpected error has occured, please copy and paste the message below and mail to
                <a href=\"mailto:[hidden email]\">[hidden email]</a><br /><br />";
               
                $this->view->message .= "<code>".$errors->exception."</code>";
                break;
        	
		}
	} 
}
