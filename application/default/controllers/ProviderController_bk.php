<?php

/**
 * ProviderController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class ProviderController extends PS_Controller_FrontAction
{
	
	function init() {
		
		
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
		//Google Rightsideadsense
		$rightside = $this->view->partial('rightside.phtml' ,array());
		$this->view->rightside = $rightside;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;
		
		//_pr($_SERVER['HTTP_REFERER'],1);

	}
	

	/**
	 * The "dashboard" action is use to display a provider lising
	 *
	 * This action to use the display provider lising.
	 
	 * via the following urls:
	 *
	 * /provider/dashboard
	 *
	 * @return void
	 */
	 /* dashboard Action Start*/
	
	
    public function dashboardAction() 
    {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		 
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );	
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);							
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_INDEX');
		
		$user_option = $objSess->user_option;
		
		if($user_option == NOTSELECTED){
		    $this->_redirect ( "/user/account" );	
		}else if($user_option != PROVIDER){
			$this->_redirect ( "/parent/dashboard" );	
		} 
		//_pr($_SESSION,1);
		$objTempSess = new Zend_Session_Namespace('temp_session');
		
		$tempUrl=$objTempSess->request_url;
		
		if(isset($tempUrl) && $tempUrl != '') {
		
			$objTempSess->request_url = SERVER_HTTP.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
	
		}

		

		$objModel = new Models_ActivityLog ();		
		//$objForm = new Models_Form_Property ();
		
		//$arrStates = $objModel->getStateCombobox();
		global $arrProviderCategory;
		global $arrStatus;
		//_pr($arrStatus,1);
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getList ( $searchText, $searchType, $sortby , $objSess->user_id );
		$pendingList = $objModel->getpendingList ( $searchText, $searchType, $sortby , $objSess->user_id );
		//_pr($pendingList,1);
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStatus = $arrStatus;
		$this->view->pendingList = $pendingList;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		//$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );			    	
    }
	
	
	/**
	 * The "add" action is use to add a landlordstep1add
	 *
	 * This action to use the add data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep1add
	 *
	 * @return void
	 */
	/*Add landlordstep1add  Action Start*/
	public function addAction() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_PROVIDER');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$objModel = new Models_ActivityLog ();
		$objForm = new Models_Form_Provider ();
		
		
		$perms = $objRequest->perms;		
		
		/* for States combo box */						
		$arrStates = $objModel->getStateCombobox();
		$objForm->state->addMultiOptions($arrStates);														
		 /*combo box end */
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			//_pr($_FILES);
			//_pr($formData,1);
			if ($objForm->isValid ( $formData )) {
				
								
				//insert the userid in database							
				$formData['user_id'] = $objSess->user_id;	
				$formData['action'] = 'add';
				$filename = $formData['name'].' '.$formData['city'].' '.$formData['state'];
				$filenameData = preg_replace('/[^a-zA-Z0-9\']/', '_', $filename);
				$filenameData = str_replace("'", '', $filenameData);
				$formData['filename'] = strtolower($filenameData);
				$formData['city'] = strtoupper($formData['city']);
				//_pr($formData,1);	
					
					//Save Formdata												
				 if($perms == 'activity') {
					$objModelafterschool = new Models_AfterschoolLog ();		 
					$provider_id = $objModelafterschool->saveData ( $formData );
				 }else{
					
					 if($perms == 'youthsport'){
						$formData['category'] = 'YOUTH-SPORTS';
					 }
					 //_pr($formData,1);
					 $provider_id = $objModel->saveData ( $formData );
					 
					 //_pr($provider_id,1);					 
				 }
				
				//_pr($provider_id,1);	
				/* multipal images upload start*/
				
				$upload = new Zend_File_Transfer_Adapter_Http();				
				$files = $upload->getFileInfo();
				$upload->setDestination(IMAGE_ROOT_PATH);
				//_pr($files,1);
				$arrUploadFile = array();$i=0;
				foreach ($files as $file => $info) {
					if($upload->isValid($file)){																																				
						$unique_file_id=uniqid().'.'.pathinfo($info['name'] ,PATHINFO_EXTENSION);

						$arrUploadFile[$i++]['imagename'] = $unique_file_id;
						$upload->addFilter('Rename',$unique_file_id,$file);
						$upload->receive($file); 
					}
				}
				

				    $objModelImages = new Models_Images ();
					 

					 for($i=0;$i<count($arrUploadFile);$i++){
						 $arrUploadFile[$i]['provider_id']  = $provider_id;
						 $objModelImages->saveData ( $arrUploadFile[$i] );
					 }
					 
				/* multipal images upload end*/

					$objError->message = $objTranslate->translate('FRONT_MSG_VALID_PROVIDER_INSERT');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/provider/dashboard");
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$this->view->perms = $perms;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Add';
		$this->view->objForm = $objForm;
	}
	/*Add Action End*/


	/**
	 * The "edit" action is use to edit a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/edit
	 *
	 * @return void
	 */
	/*Edit Pagesedit Action Start*/
	public function editAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_PROVIDER');
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Activity ();		
		$objForm = new Models_Form_Provider ();
		$objModelImages = new Models_Images ();
		$objActivityLog = new Models_ActivityLog ();	
		$objAfterschoolLog = new Models_AfterschoolLog ();
        $objAfterschool = new Models_Afterschool ();		
				
		
		//_pr($objSess->user_id,1);
		
		if($objSess->user_id == ''){
			
			//_pr($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],1);
			$objTempSess = new Zend_Session_Namespace('temp_session');
			$objTempSess->request_url = SERVER_HTTP.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
			
			$objError->message = $objTranslate->translate('FRONT_MSG_INVALID_LOGIN');
			$objError->messageType = 'error';	
			$this->_redirect ( "/user/login" );
		}
						
		$id = $objRequest->id;
		$perms = $objRequest->perms;
		$RecordTable = $objRequest->table;										
		$arrData = array ();
		
		//_pr($perms,1);
		
		if($perms == 'activity'){
		   $arrData = $objAfterschool->fetchdata ( $id );
		   $afterschool_id = $arrData['id'];
		   //_pr($arrData,1);
		   
			//get the details data
			 $introduction = $arrData['introduction'];
			 //_pr($introduction,1);
			 
			$this->view->introduction = $introduction;	
			$this->view->perms = $perms;
			
		$arrDataImage = $objModelImages->fetchMartialArtsImages( $id );	
			
		}else if($perms == 'martialarts' || $perms == 'youthsport'){
		   $arrData = $objModel->fetchdata ( $id );
		   $activity_id  = $arrData['id'];
		   //_pr($arrData,1);
			 //get the note data
			 $note = $arrData['note'];
			  
			 //get the details data
			 $details = $arrData['details'];
			 
			 
			$this->view->note = $note;
			$this->view->details = $details;
			$this->view->perms = $perms;
		
		
		$arrDataImage = $objModelImages->fetchMartialArtsImages( $id );
		
		
		}else if($RecordTable == 'activity_log'){
				
		   $arrData = $objAfterschoolLog->fetchdetails ( $id );
		   $log_id = $arrData['afterschool_id'];
		   
		   //_pr($arrData,1);
		   
			//get the details data
			 $introduction = $arrData['introduction'];
			 //_pr($introduction,1);
			 
			$this->view->introduction = $introduction;
			$this->view->RecordTable = $RecordTable;	
			
			/* featch images in image table */
			if($arrData['action'] == 'add') {
        		 $arrDataImage = $objModelImages->fetchlogImages( $id );
			 }else{
			     $arrDataImage = $objModelImages->fetchlogImages( $log_id );
			 }
			//_pr($arrDataImage,1);
			
		}else if($RecordTable == 'martialarts_log' ||$RecordTable == 'youthsport_log'){
				
		   $arrData = $objActivityLog->fetchdetails( $id ); 
		   //_pr($arrData,1);
		    $log_id  = $arrData['activity_id'];
		   //_pr($arrData,1);
			 //get the note data
			 $note = $arrData['note'];
			  
			 //get the details data
			 $details = $arrData['details'];
			 $this->view->RecordTable = $RecordTable;
			 
			$this->view->note = $note;
			$this->view->details = $details;
			
			/* featch images in image table */
			if($arrData['action'] == 'add') {
        		 $arrDataImage = $objModelImages->fetchlogImages( $id );
			 }else{
			 
			     $arrDataImage = $objModelImages->fetchlogImages( $log_id );
			 }
			//_pr($arrDataImage,1);
		}
		
		
		/* for States combo box */	
		 $arrStates = $objActivityLog->getStateCombobox();
		 $objForm->state->addMultiOptions($arrStates);														
		 /*combo box end */
		 
	
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
									
			if ($objForm->isValid ( $formData )) {
				
					
			//_pr($formData,1);
			$formData['activity_id'] = $id; 
			$formData['city'] = strtoupper($formData['city']); 
			$formData['provider_status'] = '0';
			$formData['action'] = 'update';
			//_pr($afterschool_id);
			//_pr($formData,1);
			
			if($perms == 'activity'){
				
				
 				$fetchafterschoollog = $objAfterschoolLog->fetchAfterschooldetails ( $id );
				//_pr($fetchafterschoollog,1);
				
				if(isset($fetchafterschoollog) && $fetchafterschoollog != ''){
				$objAfterschoolLog->updateAllData( $formData, $afterschool_id );
				}else{
					
					$arrData['user_id'] = $objSess->user_id;
					$arrData['afterschool_id'] = $id; 
					$arrData['city'] = strtoupper($arrData['city']);
					$arrData['provider_status'] = '0';
					$arrData['action'] = 'update';
					
					foreach($arrData as $key => $val){
						if( isset($formData[$key]) ){
							$arrData[$key] = $formData[$key];			
						}
					}
					
									
					unset($arrData['id']);
					//_pr($arrData,1);	
					$objAfterschoolLog->saveData ( $arrData );						
				}
				
				 			
				
			}else if($perms == 'martialarts' || $perms == 'youthsport'){
 				$fetchactivitylog = $objActivityLog->fetchExistRecord( $id );
				//_pr($fetchactivitylog,1);

				if(isset($fetchactivitylog) && $fetchactivitylog != ''){
					
				//_pr($arrUploadFile,1);						
				$objActivityLog->updateData ( $formData, $activity_id );
				
																		
				}else{
					
																					
					$arrData['user_id'] = $objSess->user_id;
					$arrData['activity_id'] = $id; 
					$arrData['city'] = strtoupper($arrData['city']);
					$arrData['provider_status'] = '0';
					$arrData['action'] = 'update';
					
					
					//_pr($formData,1);
					foreach($arrData as $key => $val){
						if( isset($formData[$key]) ){
							$arrData[$key] = $formData[$key];			
						}
					}
					
					//_pr($arrData,1);					
					unset($arrData['id']);
					
					$objActivityLog->saveData ( $arrData );
										 
				}
			
				 	 										
			}else if($RecordTable == 'activity_log'){
				
				if($arrData['action'] == 'add') {
						
					$formData['action'] = 'add';
				}
				
				$formData['afterschool_id'] = $log_id; 
				
				$objAfterschoolLog->updateData( $formData, $id );
						
		    }else if($RecordTable == 'martialarts_log' ||$RecordTable == 'youthsport_log'){
				
				if($arrData['action'] == 'add') {
						
					$formData['action'] = 'add';
				}
				
				   $formData['activity_id'] = $log_id;
				
				//_pr($formData,1);
				
				$objActivityLog->updatStatus ( $formData, $id );			
			
			}
			
				/* multipal images upload start*/
					
				$upload = new Zend_File_Transfer_Adapter_Http();				
				$files = $upload->getFileInfo();
				$upload->setDestination(IMAGE_ROOT_PATH);
				//_pr($files,1);
				$arrUploadFile = array();$i=0;
				foreach ($files as $file => $info) {
					if($upload->isValid($file)){																																				
						$unique_file_id=uniqid().'.'.pathinfo($info['name'] ,PATHINFO_EXTENSION);

						$arrUploadFile[$i++]['imagename'] = $unique_file_id;
						$upload->addFilter('Rename',$unique_file_id,$file);
						$upload->receive($file); 
					}
				}
				
				/* echo 'afterschool_id'.'<br/>';
				_pr($formData['id']);
				echo 'activity_id'.'<br/>';
				_pr($log_id,1);*/
				
				 for($i=0;$i<count($arrUploadFile);$i++){
					 
					 if($perms == 'activity' || $perms == 'martialarts' || $perms == 'youthsport'){
					 $arrUploadFile[$i]['provider_id']  = $formData['id'];
					 }else{
						if($arrData['action'] == 'add') {
							$arrUploadFile[$i]['provider_id']  = $arrData['id'];
						}else {
					        $arrUploadFile[$i]['provider_id']  = $log_id;
					   }
					 }
					 $objModelImages->saveData ( $arrUploadFile[$i] );
				 }
				/* multipal images upload end*/
				
														
				$objError->message = $objTranslate->translate('FRONT_MSG_VALID_PROVIDER_INSERT');
				$objError->messageType = 'confirm';	
				$this->_redirect ( "/provider/dashboard" );											
			}else {	
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}			
		
		}else{
			//Auto populate the records
			$objForm->populate ( $arrData );
		} 
					
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->objForm = $objForm;
		$this->view->arrDataImage = $arrDataImage;
		$this->view->id = $id;
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );
	}
	/*Edit Pagesedit Action End*/




	/**
	 * The "delete" action is use to delete a image
	 *
	 *	This action to use the delete image
	 * via the following urls:
	 *
	 * /property/landlordstep4
	 *
	 * @return void
	 */
	/*Delete imagedelete Action Start*/
	public function deleteimageAction() {
	
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objModel = new Models_Images();						
		//$objModel = new Models_Activity ();	
        $objAfterschool = new Models_Afterschool ();			
		$objActivityLog = new Models_ActivityLog ();	
		$objAfterschoolLog = new Models_AfterschoolLog ();				

		$id = $objRequest->id;
		$perms = $objRequest->perms;
		$RecordTable = $objRequest->table;	
		//echo $RecordTable; exit;						
		$arrData = array ();
		$arrData = $objModel->fetchimage ( $id );
	   
		
		if ($perms == 'activity'){
			
			$afterschoolid = $arrData['provider_id'] ;
			$afterschoolarrData = array ();
			$afterschoolarrData = $objAfterschoolLog->fetchAfterschooldetails ( $afterschoolid );
			//_pr($afterschoolarrData,1);	
				$afterschoolarrData['provider_status'] = '0';
			
				$objAfterschoolLog->updateAllData ( $afterschoolarrData, $afterschoolid );
	  
	   }else if($perms == 'martialarts' || $perms == 'youthsport'){	
	
			$activityid = $arrData['provider_id'] ;
			$activityarrData = array ();
			$activityarrData = $objActivityLog->fetchExistRecord ( $activityid );
			_pr($activityarrData,1);
				$activityarrData['provider_status'] = '0';

				$objActivityLog->updateData ( $activityarrData, $activityid );

		}else if (isset($RecordTable) && $RecordTable== 'activity_log'){
			
			$afterschoolid = $arrData['provider_id'] ;
			$afterschoolarrData = array ();
			$afterschoollogarrData = $objAfterschoolLog->fetchAfterschooldetails ( $afterschoolid );
			//_pr($afterschoollogarrData,1);			
				$afterschoolarrData['provider_status'] = '0';
	
			  	$objAfterschoolLog->updateAllData ( $afterschoolarrData, $afterschoolid );
	
		
	   }else if($RecordTable == 'martialarts_log' || $RecordTable == 'youthsport_log'){	
	
			$activityid = $arrData['provider_id'] ;
			$activityarrData = array ();
			$activityarrData = $objActivityLog->fetchExistRecord ( $activityid );
			//_pr($activityarrData,1);
				$activityarrData['provider_status'] = '0';
				
				$objActivityLog->updateData ( $activityarrData, $activityid );
			
		}
		
		$arrData['approved'] = '-1';
		
		$objModel->updateImagesstatus ( $arrData, $id );
		
	   /* $dirImageRootPath= IMAGE_ROOT_PATH;

		$file = $dirImageRootPath.$arrData['imagename'];
		
		unlink($file);
		
		if(empty($arrData['imagename']) && empty($arrData['imagename']))
			rmdir($dirImageRootPath.$id);
		
		$objModel->deleteimages ( $id );*/
												
		$objError->message = $objTranslate->translate('ADMIN_MSG_SUCCESS_IMAGE_DELETED');
		$objError->messageType = 'confirm';	/*activity*/
		
		if ($perms == 'activity'){
		   $this->_redirect ( "/provider/edit/perms/activity/id/".$arrData['provider_id']);
		}
		else if($perms == 'martialarts'){	
		   $this->_redirect ( "/provider/edit/perms/martialarts/id/".$arrData['provider_id']);				
		} else if($perms == 'youthsport'){
		   $this->_redirect ( "/provider/edit/perms/youthsport/id/".$arrData['provider_id']);	
	    } else if($RecordTable == 'activity_log'){
			$this->_redirect ( "/provider/edit/table/activity_log/id/".$afterschoollogarrData['id']);
		} else if($RecordTable == 'martialarts_log'){
			$this->_redirect ( "/provider/edit/table/martialarts_log/id/".$activityarrData['id']);
		}else if($RecordTable == 'youthsport_log'){
			$this->_redirect ( "/provider/edit/table/youthsport_log/id/".$activityarrData['id']);
		}
		exit;
	}
	/*Delete Action End*/

		
}
?>
