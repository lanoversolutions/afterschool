<?php

class Admin_IndexController extends PS_Controller_Action  
{ 

 /*----------------| Index Action |---------------------*/
   public function indexAction()
    {    	
    	$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);
		
		$this->view->messageType = $objError->messageType;
		$this->view->message = $objError->message;
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LOIGN');

		$objRequest = $this->getRequest();
		
		$objModel = new Models_AdminLogin() ;
		$objForm = new Models_Form_AdminLogin();
		
			
        if ($objRequest->isPost()) {
            $formData = $objRequest->getPost();

            if ($objForm->isValid($formData)){

            	if(isset($formData['submit'])) {
                    $isValid = $objModel->verifyLoginInfo($formData);
                   
                    if ($isValid) {
                    	$objModel->setSession();
					    $objError->messageType = 'confirm';
                        $this->_redirect("/admin/welcome/");
                    } else {
	                    $objError->message = $objTranslate->translate('ADMIN_MSG_INVALID_LOGIN');
	                    $objError->messageType = 'error';
                        $this->_redirect("/admin/");
                    }
                }
            }
            else
            {
                $objForm->populate($formData);
                $objError->message = formatErrorMessage($objForm->getMessages());                
                $objError->messageType = 'error';
            }
           
        }
//        $actionName = $this->actionName;
        $this->view->message = $objError->message;
        $this->view->messageType = $objError->messageType;
        $objSess = new Zend_Session_Namespace(PS_App_Auth);
        $this->view->user_id =  $objSess->user_id;
        $objError->message  = "";
		$objError->messageType = "";
		 
        $this->view->objForm = $objForm;
        $this->view->leftPanelFlag	= '';
        $this->_helper->layout->setLayout('adminlogin');
    }
        

 /*----------------| logout Action |---------------------*/    
    public function logoutAction()
    {	
        $objModel = new Models_AdminLogin();						
        $objModel->clearSession();
		$this->_redirect("/admin/");
    }			
	
	
	/* Change Language Action */
	public function changelangAction(){		
		$objSess = new Zend_Session_Namespace(PS_App_Auth);
		$objRequest = $this->getRequest();
			
		if(isset($objRequest->lang) && $objRequest->lang != ''){			
			$objSess->app_lang = $objRequest->lang;												
		}
		$this->_redirect($_SERVER['HTTP_REFERER']);
	}
	

}
?>