<?php

class Admin_GeneralsettingsController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
	
	/**
	 * The "edit" action is use to edit a GeneralSettings
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/edit
	 *
	 * @return void
	 */
	/*Edit GeneralSettingsedit Action Start*/
	public function editAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_GENERALSETTINGS_EDIT');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_HEADING_GENERALSETTINGS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Generalsettings ();		
		$objForm = new Models_Form_Generalsettings ();
		$arrData =  $objModel->getGeneralSettings();
				
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchEntry ( $id );																
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
									
			if ($objForm->isValid ( $formData )) {				
				$objModel->updateData ( $formData, $id );
				$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_GENERALSETTINGS_UPDATE');
				$objError->messageType = 'confirm';	
				$this->_redirect ( "/admin/generalsettings/edit/id/1" );											
			}else {	
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}			
		}else{
			//Auto populate the records
			$objForm->populate ( $arrData );
		} 
					
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->objForm = $objForm;
		$this->view->id = $id;
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );
	}
	/*Edit GeneralSettingsedit Action End*/
		
}
?>