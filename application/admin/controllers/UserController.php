<?php

class Admin_UserController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
	
				 
	/* index Action */
	public function indexAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_User ();		
		//$objForm = new Models_Form_Frontuser();
		
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getList ( $searchText, $searchType, $sortby );
		
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		//$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	
	
	
	/**
	 * The "edit" action is use to edit a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/edit
	 *
	 * @return void
	 */
	/*Edit Pagesedit Action Start*/
	public function changestatusAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_User ();		
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchEntry ( $id );
		
		if(!empty($arrData)){

				if($arrData['user_status'] == 0 ){
					$formData['user_status']='1';
				}else{
					$formData['user_status']='0';
				}

			$objModel->updateData ( $formData, $arrData['user_id'] );
			
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_USER_UPDATE');
			$objError->messageType = 'confirm';	
			$this->_redirect ( "/admin/user" );
		} else {
			$objError->message = formatErrorMessage ( $objForm->getMessages () );
			$objError->messageType = 'error';
			$this->_redirect ( "/admin/user" );											
		}
				
		//exit;		
	}
	/*Edit Pagesedit Action End*/
	

		/**
	 * The "Propertydetails" action is use to display a propertydetails
	 *
	 * This action to use the display property details.
	 
	 * via the following urls:
	 *
	 * /property/propertylisting/propertydetails
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/

	public function userdetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER_DETAILS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_User ();
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchEntry ( $id );
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->currentAction = 'userdetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}

	
	
	
	
	/**
	 * The "delete" action is use to delete a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/delete
	 *
	 * @return void
	 */
	/*Delete Pagesdelete Action Start*/
	public function deleteAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objModel = new Models_User ();						
		
		$id = $objRequest->id;						
		$objModel->deleteData($id);		
				
		$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_USER_DELETE');
		$objError->messageType = 'confirm';		
		$this->_redirect ( "/admin/user" );				
		exit;
	}
	/*Delete Pagesdelete Action End*/
	
}
?>