<?php

class Admin_ErrorController extends PS_Controller_Action
{
    public function accesscontrolAction()
    {
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		
		$this->view->title=$objTranslate->translate('HEAD_ERROR_ACCESSDENIED');
		
		$this->view->message = $objTranslate->translate('MSG_ERROR_ACCESSDENIED');
    }
}
