<?php

class Admin_ReviewController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
	
				 
	/* index Action */
	public function reviewdashboardAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_REVIEW');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_REVIEW');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Reviews();		
		//$objForm = new Models_Form_ActivityLog();
	   //$objForm->Listsearch();
		global $arrRating_Type;
		
		$objActivity_log = new Models_ActivityLog();		
		$arrStates = $objActivity_log->getStateCombobox();
		//_pr($arrStates,1);
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getreviewList( $searchText, $searchType, $sortby );
		
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->arrRating_Type = $arrRating_Type;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;
		$this->view->currentAction = 'activity';
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	
		/**
	 * The "reviewdetail" action is use to display a review
	 *
	 * @return void
	 */
	 /* reviewdetail Action Start*/

	public function reviewdetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_REVIEW_DETAILS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Reviews ();
		
		global $arrRating_Type;
		
		$id = $objRequest->id;
		$arrData = array ();
		$arrData = $objModel->fetchdetails( $id );

		$this->view->arrRating_Type = $arrRating_Type;
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->currentAction = 'activitydetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}



		/**
	 * The "approve" action is use to approve a review
	 *
	 * @return void
	 */
	 /* approve Action Start*/

	public function approveAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Reviews ();
		
		$id = $objRequest->id;
		$status = $objRequest->status;	
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );
		//_pr($arrData,1);
		
		$provider_id = $arrData['provider_id'];
		
		if($status == 'approve'){
			
			$formData['approved'] = '1';
			
			$objModel->updateData ( $formData, $id ,$provider_id);
			
						
		}else if($status == 'disapprove'){
			
			$formData['approved'] = '-1';
			//_pr($formData,1);
			$objModel->updateData ( $formData, $id ,$provider_id );
			
		}
		
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
			$objError->messageType = 'confirm';
			$this->_redirect ( "/admin/Review/reviewdashboard");
		
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'approve';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	
		
}
?>