<?php
 
/**
 * SettingController 
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */ 
 
class Admin_SettingController extends PS_Controller_Action
{ 
   	
	/**
     * The "setting" action is for add default configurations for site .
     *
     * Assuming the default route and default router, this action is dispatched 
     * via the following urls:
     *   /
     *   /setting/
     *   /setting/setting
     *
     * @return void
     */
	
   //Edit settings starts
   function indexAction()
	{
		/*
		$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$actionName     = $this->getRequest()->getActionName();
        $controllerName = $this->getRequest()->getControllerName();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		$this->view->siteTitle=$objTranslate->_("TITLE_SITE_SETTING");

		$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);
		$objModel = new Models_GeneralSetting;
		$objForm =	new Models_Form_GeneralSetting;
				  		
		$arrData =  $objModel->getGeneralSettingRec();
		$objForm->settingForm($arrData);
		
		$objRequest = $this->getRequest();
		
		if( $objRequest->isPost())
		{
			$formData = $objRequest->getPost();
			
			
			if($objForm->isValid($formData))
			{
			
				foreach( $formData  as $key =>  $val  )
					if( $key != 'submit' )
						$objModel->updateGernalSettingRec( $key, $val);
					
				$objError->message = $objTranslate->translate('RECORD_UPDATE_SUCCESSFULLY') ;
				$objError->messageType = 'confirm';
				
			}
			else
			{
			
				$objError->message = formatErrorMessage($objForm->getMessages());			
				$objError->messageType = 'error';
			}
		}		
		
		$objForm->submit->setLabel($objTranslate->translate('BUTTON_LABEL_UPDATE'));
		$this->view->arrData =$arrData; 
		$this->view->objForm = $objForm; 
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = '';
		
		unset($objForm,$objError,$objModel,$objTranslate); 	
	*/
	}
	
	//Edit settings Ends
}
