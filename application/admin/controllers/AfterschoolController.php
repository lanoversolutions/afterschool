<?php
/**
 * AfterschoolController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class Admin_AfterschoolController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
	
				 
	/* index Action */
	public function activitydashboardAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_ACTIVITY');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_ACTIVITY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Activity();		
		$objForm = new Models_Form_ActivityLog();
		
		$objActivity_log = new Models_ActivityLog();		
		$arrStates = $objActivity_log->getStateCombobox();
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getactivityList( $searchText, $searchType, $sortby );
		
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;
		$this->view->objForm = $objForm;					
		$this->view->currentAction = 'activity';
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	
		/**
	 * The "activitydetails" action is use to display a activitydetails
	 *
	 * This action to use the display activity details.
	 *
	 * @return void
	 */
	 /* activitydetails Action Start*/

	public function activitydetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_ACTIVITY_DETAILS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_ActivityLog ();
		$objImages = new Models_Images();
		
		$id = $objRequest->id;
														
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );

		$AvtivityAction = $arrData['action'];
		$activity_id = $arrData['activity_id'];
		$objActivity = new Models_Activity ();	
		$activity = array ();
		$activity =  $objActivity->fetchactivitydetails($activity_id);
		
		
			if($arrData['category'] == 'MARTIAL-ARTS'){
			    $image_type = 'MARTIAL-ARTS';
			}else{
			    $image_type = 'YOUTH-SPORTS';
			}
		
		$ApprovedImages =  $objImages->fetchactivityImages($activity_id,$image_type);
		//_pr($ApprovedImages,1);
		
		if($AvtivityAction == 'add'){
		 $image_id = $id;
		}else{
		 $image_id = $activity_id;
		}
		$Images =  $objImages->fetchImages($image_id,$image_type);
		$arrStates = $objModel->getStateCombobox();
		
		$this->view->ApprovedImages = $ApprovedImages;
		$this->view->ImagesList = $Images;	
		$this->view->AvtivityAction = $AvtivityAction;			
		$this->view->arrDataList = $arrData;
		$this->view->activity = $activity;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'activitydetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}



		/**
	 * The "approve" action is use to approve a activity data
	 *
	 * @return void
	 */
	 /* approve Action Start*/

	public function approveAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_ActivityLog ();
		$objImages = new Models_Images();
		
		$id = $objRequest->id;
		$status = $objRequest->status;	
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );
		//_pr($arrData,1);
		$arrStates = $objModel->getStateCombobox();
		
			/*if($arrData['category'] == 'MARTIAL-ARTS'){
			    $image_type = 'MARTIAL-ARTS';
			}else{
			    $image_type = 'YOUTH-SPORTS';
			}*/

		
		if($status == 'approve'){

			$objActivity = new Models_Activity();	
			unset($arrData['id']);	
			
				$arrData['approved'] = '1';
			
			if($arrData['action'] == 'add'){
			
				$activity_id = $objActivity->saveData ( $arrData );
			
				$city = $arrData['city'];
				$state = $arrData['state'];
			
			$objCityModel = new Models_Cities();
			$arrayCities = array ();
				$arrayCities = $objCityModel->fetchCityEntry($city,$state);
				$cityId = $arrayCities['id'];
			
			if($arrData['category'] == 'YOUTH-SPORTS'){
			
				$arrayCities['sports_count'] = $arrayCities['sports_count'] + 1; 
				
			}else if($arrData['category'] == 'MARTIAL-ARTS'){
			
				$arrayCities['activities_count'] = $arrayCities['activities_count'] + 1; 	
				
			}

			$objCityModel->updatCount ($arrayCities, $cityId );
					
				$formData['activity_id'] = $activity_id;
			
			
			/* update image section start*/
			$imagedata['provider_id'] = $activity_id;
			
			if($arrData['category'] == 'MARTIAL-ARTS'){
				$imagedata['type'] = 'MARTIAL-ARTS';
			}else{
				$imagedata['type'] = 'YOUTH-SPORTS';
			}
			$imagedata['approved'] = '1';
			
			$objImages->updateImages( $imagedata ,$imagedata['type'] ,$id );
		    
			$image_provider_id = $imagedata['provider_id'];
			
			$activity_image_log_path = ACTIVITY_ROOT_IMAGE_PATH.'Log_'.$id;
			$activity_image_path = ACTIVITY_ROOT_IMAGE_PATH.$image_provider_id;
						
			$arrayimages = $objImages->fetchImages($image_provider_id,$imagedata['type']);
			
			if(!empty($arrayimages)){
				//echo $activity_image_path;is_dir($activity_image_path);exit;
				if(!is_dir($activity_image_path)){
					mkdir($activity_image_path);	
				}
			}
			foreach( $arrayimages as $imagesdata ):
				
				if(file_exists($activity_image_log_path.'/'. $imagesdata['imagename'])){
					$success = copy($activity_image_log_path.'/'.$imagesdata['imagename'],$activity_image_path.'/'.$imagesdata['imagename']);
					if($success)
						unlink($activity_image_log_path.'/'.$imagesdata['imagename']);		
				}
				
				
			endforeach;
			
			if(is_dir($activity_image_log_path)):
				rmdir($activity_image_log_path);
			endif;
			
			/* update image section end */
			//exit;
			
			} else if ($arrData['action'] == 'update'){
			
			$activity_id = $arrData['activity_id'];
				
			$objActivity->updateactivityData ( $arrData, $activity_id );
			
			/* update image section start*/
			$imagedata['provider_id'] = $activity_id;
			if($arrData['category'] == 'MARTIAL-ARTS'){
				$imagedata['type'] = 'MARTIAL-ARTS';
			}else{
				$imagedata['type'] = 'YOUTH-SPORTS';
			}			
			$imagedata['approved'] = '1';

			$objImages->updateImages( $imagedata ,$imagedata['type'] ,$activity_id );
			/* update image section end */
			}
			
			$formData['provider_status'] = '1';

			$objModel->updatStatus ( $formData, $id );	
			
						
		}else if($status == 'disapprove'){
			
			$formData['provider_status'] = '-1';

			$objModel->updatStatus ( $formData, $id );
			
		}
		
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
			$objError->messageType = 'confirm';
			$this->_redirect ( "/admin/Afterschool/activitydashboard");
		
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'approve';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	
	

	/* afterschooldashboard Action */
	public function afterschooldashboardAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_AFTERSCHOOL');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_AFTERSCHOOL');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_AfterschoolLog ();		
		
		$objActivity_log = new Models_ActivityLog();		
		$arrStates = $objActivity_log->getStateCombobox();
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getList ( $searchText, $searchType, $sortby );
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		//$this->view->objForm = $objForm;
		$this->view->currentAction = 'afterschooldashboard';
		unset ( $objModel, $objSelect, $objPaginator );	
	}
		




		/**
	 * The "afterschooldetails" action is use to display a afterschooldetails
	 *
	 * This action to use the display afterschool details.
	 *
	 * @return void
	 */
	 /* afterschooldetails Action Start*/
	public function afterschooldetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_AFTERSCHOOL_DETAILS');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_AfterschoolLog();
		$objImages = new Models_Images();
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchdetails( $id );

		$AfterschoolAction = $arrData['action'];
		$afterschool_id = $arrData['afterschool_id'];
		$objAfterschool = new Models_Afterschool();	
		$afterschool = array ();
		$afterschool =  $objAfterschool->fetchafterschooldetails($afterschool_id);
		
 		$image_type = 'AFTERSCHOOL';
		
		$ApprovedImages =  $objImages->fetchactivityImages($afterschool_id,$image_type);
		
		if($AfterschoolAction == 'add'){
		 $image_id = $id;
		}else{
		 $image_id = $afterschool_id;
		}
		$Images =  $objImages->fetchImages($image_id,$image_type);
		
		$objActivity_log = new Models_ActivityLog();		
		$arrStates = $objActivity_log->getStateCombobox();


		$this->view->ApprovedImages = $ApprovedImages;
		$this->view->ImagesList = $Images;			
		$this->view->afterschool = $afterschool;			
		$this->view->arrDataList = $arrData;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->arrStates = $arrStates;	
		$this->view->id = $id;
		$this->view->currentAction = 'afterschooldetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	/*afterschooldetails Action End*/
	


		/**
	 * The "approveafterschool" action is use to approve a afterschool data
	 *
	 * @return void
	 */
	 /* approveafterschool Action Start*/

	public function approveafterschoolAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_AfterschoolLog ();
		$objImages = new Models_Images();
		
		$id = $objRequest->id;
		$status = $objRequest->status;	
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );
		
		$objActivity_log = new Models_ActivityLog();
		$arrStates = $objActivity_log->getStateCombobox();
			
			//$image_type = 'AFTERSCHOOL';
			
		if($status == 'approve'){

			$objAfterschool = new Models_Afterschool();	
			unset($arrData['id']);	
			
			$arrData['approved'] = '1';
			
			if($arrData['action'] == 'add'){
			
			$afterschool_id = $objAfterschool->saveData ( $arrData );
			
			
			$city = $arrData['city'];
			$state = $arrData['state'];
			
			$objCityModel = new Models_Cities();
			$arrayCities = array ();
			$arrayCities = $objCityModel->fetchCityEntry($city,$state);
			$cityId = $arrayCities['id'];
			
			if($arrData['category'] == 'AFTERSCHOOL'){
			
			$arrayCities['afterschool_count'] = $arrayCities['afterschool_count'] + 1; 
				
			}

			$objCityModel->updatCount ($arrayCities, $cityId );
						
			
			$formData['afterschool_id'] = $afterschool_id;
			
			/* update image section start*/
			$imagedata['provider_id'] = $afterschool_id;
			$imagedata['type'] = 'AFTERSCHOOL';
			$imagedata['approved'] = '1';
			$objImages->updateImages( $imagedata,$imagedata['type'] , $id );
			/* update image section end */
			$image_provider_id = $imagedata['provider_id'];
			
			$afterschool_image_log_path = AFTERSCHOOL_ROOT_IMAGE_PATH.'Log_'.$id;
			$afterschool_image_path = AFTERSCHOOL_ROOT_IMAGE_PATH.$image_provider_id;
						
			$arrayimages = $objImages->fetchImages($image_provider_id,$imagedata['type']);
			
			if(!empty($arrayimages)){
				//echo $activity_image_path;is_dir($activity_image_path);exit;
				if(!is_dir($afterschool_image_path)){
					mkdir($afterschool_image_path);	
				}
			}
			foreach( $arrayimages as $imagesdata ):
				
				if(file_exists($afterschool_image_log_path.'/'. $imagesdata['imagename'])){
					$success = copy($afterschool_image_log_path.'/'.$imagesdata['imagename'],$afterschool_image_path.'/'.$imagesdata['imagename']);
					if($success)
						unlink($afterschool_image_log_path.'/'.$imagesdata['imagename']);		
				}
				
			endforeach;
			
			if(is_dir($afterschool_image_log_path)):
				rmdir($afterschool_image_log_path);
			endif;
				
			
			} else if ($arrData['action'] == 'update'){
			
			$afterschool_id = $arrData['afterschool_id'];
				
			$objAfterschool->updateafterschoolData ( $arrData, $afterschool_id );
			
			
			/* update image section start*/
			$imagedata['provider_id'] = $afterschool_id;
			$imagedata['type'] = 'AFTERSCHOOL';
			$imagedata['approved'] = '1';
			$objImages->updateImages( $imagedata,$imagedata['type'] , $afterschool_id );
			/* update image section end */
							
			}
			
			$formData['provider_status'] = '1';

			$objModel->updatStatus ( $formData, $id );	
			
		}else if($status == 'disapprove'){
			
			$formData['provider_status'] = '-1';
			
			
			$objModel->updatStatus ( $formData, $id );
			
		}
		
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
			$objError->messageType = 'confirm';
			$this->_redirect ( "/admin/Afterschool/afterschooldashboard");
		
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'approve';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	
	
	
	
	
	
}
?>