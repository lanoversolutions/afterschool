<?php
	///////////////////////
	// SITE CONFIGURATION//
	///////////////////////
	$path_http = pathinfo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	//define("SERVER_PATH", $path_http["dirname"]."/"); 								// server path is deined here
	$arrDirPath = explode("/", $path_http["dirname"]);
	if($arrDirPath[count($arrDirPath)-1] == "admin" || $arrDirPath[count($arrDirPath)-1] == "trunk"){
		define("SERVER_ROOT_PATH", substr(getcwd(), 0, (strlen(getcwd())-strlen($arrDirPath[count($arrDirPath)-1])))); // server root path is deined here
		$serverPath = $arrDirPath;
		array_pop($serverPath);
		$serverUrl = implode("/",$serverPath);
		define("SERVER_PATH", $serverUrl."/"); 		 					// server path is deined here
	}else{
		define("SERVER_ROOT_PATH", getcwd()."/"); 		  		// server root path is deined here
		$serverUrl = implode("/",$arrDirPath);
		define("SERVER_PATH", $serverUrl."/"); 								// server path is deined here
		$path_https = pathinfo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	}

	$path_https = pathinfo('https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	
	if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") 
		define('SERVER_HTTP','https://');	
	else
		define('SERVER_HTTP','http://');
	/*Email Send Configuration Detail*/
	define('CONFIGURATION_EMAIL_FROM','huyv76@gmail.com');
	define('CONFIGURATION_NAME_FROM','Huy Vo Admin');
	
	define('NEW_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/review/');
	
	define('EMAIL_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/user/');
	define('EMAIL_CONTACT_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/contact/');
    define('EMAIL_TENANTCONTACT_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/tenant/');

	/**/
	
	define("SERVER_SSL_PATH", $path_https["dirname"]."/");
	
	/* Site Namespace Variables
	 */
	define('PS_App_Zend_Translate','Zend_Translate');
	define('PS_App_Error','PS_Error');
	define('PS_App_Auth','PS_Auth');
	define('PS_App_Configuration','configuration');
	
	define('PS_App_DbAdapter','DbAdapter');	
	//define('PS_App_DbAdapter_Master','DbAdapterMaster');
	//define('PS_App_DbAdapter_Slave','DbAdapterSlave');
	
	define('PS_App_Acl','acl');
	define('PS_App_User_Info','PS_App_User_Info');
	define('PS_Front_App_Auth','PS_Front_App_Auth');
	
	/* Site Path Variables */					
	#---  JAVASCRIPT path ---#
	define('JS_PATH',SERVER_PATH.'js/');
	define('JS_ROOT_PATH',SERVER_ROOT_PATH.'js/');
	
	#----------email---------------#
	define('EMAIL_ROOT_PATH',SERVER_ROOT_PATH.'js/');
	
		#---  STYLE path ---#
	define('CKFINDER_ROOT_PATH',SERVER_ROOT_PATH.'ckfinder/');
	define('CKFINDER_PATH',SERVER_PATH.'ckfinder/');

	#---  STYLE path ---#
	define('STYLE_PATH',SERVER_PATH.'styles/');
	define('STYLE_ROOT_PATH',SERVER_ROOT_PATH.'styles/');	
	
	#---  IMAGE path ---#
	define('IMAGE_PATH',SERVER_PATH.'assets/images/');
	define('IMAGE_ROOT_PATH',SERVER_ROOT_PATH.'assets/images/');
	
	#--- AFTERSCHOOL IMAGE path ---#
	define('AFTERSCHOOL_IMAGE_PATH',SERVER_PATH.'assets/afterschool/');
	define('AFTERSCHOOL_ROOT_IMAGE_PATH',SERVER_ROOT_PATH.'assets/afterschool/');
	
	#--- ACTIVITY IMAGE path ---#
	define('ACTIVITY_IMAGE_PATH',SERVER_PATH.'assets/activity/');
	define('ACTIVITY_ROOT_IMAGE_PATH',SERVER_ROOT_PATH.'assets/activity/');
	
	#---  CAPTCHA IMAGE path ---#
	define('CAPTCHA_PATH',SERVER_PATH.'captcha/');
	
		#---  IMAGE path ---#
	define('USER_IMAGES_PATH',SERVER_PATH.'img/');
	define('USER_IMAGES_ROOT_PATH',SERVER_ROOT_PATH.'img/');

	#--- Patient Direcetory Path ---#
	define('ACTIVATION_USER_CODE_PATH',SERVER_PATH.'default/user/activation/confirm_key/');
	
	#--- Patient Direcetory Path ---#
	define('CHECK_FORGOTPASS_PATH',SERVER_PATH.'default/user/checkpass/confirm_key/');
	
	#--- Invited person email activation Direcetory Path ---#
	define('ACTIVATION_INVITED_USER_CODE_PATH',SERVER_PATH.'default/register/eactivation/acode/');
	
	#---  USER IMAGE path ---#
	define('USER_IMAGE_PATH',IMAGE_PATH.'user/');
	define('USER_IMAGE_ROOT_PATH',IMAGE_ROOT_PATH.'user/');	
	
		#--- Patient Direcetory Path ---#
	define('GENARAL_MENU_PATH',SERVER_PATH.'application/views/scripts/property/');
				
							
	#---  Assets directory path ---#
	define('ASSETS_PATH',SERVER_PATH.'assets/');
	define('ASSETS_ROOT_PATH',SERVER_ROOT_PATH.'assets/');
	
	#--- Patient Direcetory Path ---#
	define('PATIENT_DOC_ROOT_PATH',ASSETS_ROOT_PATH.'patient_doc/');
			
	#---  Force download directory path ---#	
	define("FORCE_DOWNLOAD_PATH", ASSETS_PATH.'download.php');
	
		
	#--- GLOBAL ARRAYS ---#           
    // PAGING PARAM SETTINGS
    define('TOTAL_RECORDS_PER_PAGE',  '2');
    define('TOTAL_PAGE_IN_GROUP',  '3');
	
	
	#---  ARRAYS ---#

    //Property_Type array
	$arrRating_Type  = array(''=>'Select your Rating',
				'1'=>'1 star',
				'2'=>'2 star',
				'3'=>'3 star',
				'4'=>'4 star',
				'5'=>'5 star');

	$arrUserOption  = array('1'=>'parent','2'=>'provider');
	
	$arrProviderCategory  = array(''=>'Select your Category',
				'MARTIAL-ARTS'=>'MARTIAL-ARTS',
				'YOUTH-SPORTS'=>'YOUTH-SPORTS');
				
	$arrStatus  = array(
				'0'=>'pending',
				'1'=>'Accept',
				'-1'=>'Reject');			

	$arrClaim  = array(
				'1'=>'Yes',
				'0'=>'NO');


	$arrProviderStatus  = array(''=>'Select your Status',
				'Full Permit'=>'Full Permit',
				'License is not Renewed'=>'License is not Renewed',
				'Open'=>'Open',
				'Licensed' => 'Licensed'
				);			

	
	/*Define User type*/	
    define('USER_TYPE_FREE', 'FREE');
	define('USER_TYPE_PAIDTYPE1', 'PAIDTYPE1');
	
	/*Define Active & not active Variables*/
	define('USER_ACTIVE', '1');
	define('USER_NOT_ACTIVE', '0');
	
	/*User type*/
	define('NOTSELECTED', '0');
	define('PARENT', '1');
	define('PROVIDER', '2');
	define('REQUEST_URI',SERVER_HTTP.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);