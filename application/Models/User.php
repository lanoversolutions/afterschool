<?php

/**
 * Class for User table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_User extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'front_user'; 		
                   
	/**
     * Get all user listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='')
	{ 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "f.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('f'=>'front_user'),array('f.*'));
				//->where('user_option != 0');
							 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('f.user_id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
  
  	
	 /**
     * Fetch Particular user Detail
     *	 
     */
    /*----------------------| Get spacific user Detail   |----------------------*/
    public function fetchEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.*'))								
				->where('f.user_id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }
	
	
	/**
    * Save data for new user entry.
    *
    * @param  array $data Array of user data to insert in database.
    * @return integer|boolean Last inserted user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }
		
		if(isset($data['username']) && $data['username'] != "") {
			$encPwd = md5($data['username'].'1');
			$data['password'] = $encPwd;
		}		                        
        $data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        						
		//for send email notification
		$urlhelper = Zend_Controller_Action_HelperBroker::getStaticHelper('url');
		$serverUrlHelper = new Zend_View_Helper_ServerUrl();
		$url = $serverUrlHelper->serverUrl(	
			$urlhelper->url(
				array('module'=>'admin',
					  'controller'=>'user',
					  'action'=>'index'					  
					  )
				,null,
				true
			) 
		);					
		$from = 'noreply@leadmanagement.in';
		$to = $data['email'];
		$subject = 'User Registration Notification';
		$body = 'Please, click on below link to login <br>';																					
		$body .= '<a href="'.$url.'">Login here</a>'.'<br>';
		$body .= 'UserName:'.$data['username'].'<br>';
		$body .= 'Password:'.$data['username'].'1';																															
		$headers = "From:" . $from;
		mail($to,$subject,$body,$headers); 	
																		
        return $this->insert($data);
     }
			 	     

    /**
     * Update data for user entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "user_id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
    
	
	/**
     * Delete data forom user entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
  	public function deleteData($id) {
    	$objData = $this->fetchRow('user_id='.$id);	
		if(!empty($objData)){
    		$objData->delete();
    	}	
    	unset($objData);
  	}

  	/**
     * Delete more than one row data forom user entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteAllData($id_arr) {									
		$cnt_ids=count($id_arr);
		$str_id='';
		$i=0;
		foreach ($id_arr as $x){
			if($i==$cnt_ids-1){
				$str_id.=$x;
			}else{
				$str_id.=$x.',';	
			}
			$i++;
		}
		$str_id='id IN ('.$str_id.')';
		
		$objData = $this->fetchRow($str_id);		
		$objData->delete($str_id);
		unset($objData);
	}
  	
	
	 /**
     * create user cmbo box
     *	 
     */
    /*----------------------| create user cmbo box |----------------------*/
    public function getUserCombo() {
		$objTranslate = Zend_Registry::get('Zend_Translate');      	
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('u'=>'user'),array('u.id','u.name'))								
				->where('u.is_admin != 1');										
    	
    	$select = $this->fetchAll($select);
		$select = $select->toArray();    									
		
			$arrData = array();
			$arrData[0] = $objTranslate->translate('ADMIN_DEFAULT_COMBO_USER');
			foreach($select as $row){
				$arrData[$row['id']] = $row['name'];	
			}				
						
		if($arrData)
	    	return $arrData;
		else
      		return null;    
    }
 	
}
?>
