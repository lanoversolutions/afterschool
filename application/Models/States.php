<?php

/**
 * Class for States table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_States extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'states'; 		
                   

	
	 /**
     * Fetch All states
     *	 
     */
    /*----------------------| Get All states Detail   |----------------------*/
    public function fetchcountry() {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.*'))								
				->where('s.country = ?','us');
				
    	
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	

	 /**
     * Fetch All states
     *	 
     */
    /*----------------------| Get spacific states Detail   |----------------------*/
    public function fetchstatename($statename) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.*'))								
				->where('s.statefile = ?',$statename);
				
		$select = $this->fetchrow($select);
		//_pr($select,1);
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	
	
	 /**
     * Fetch All states
     *	 
     */
    /*----------------------| Get spacific states Detail   |----------------------*/
    public function fetchstatelisting($statecode) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.*'))								
				->where('s.state_code = ?',$statecode);
		//echo $select; exit;		
		$select = $this->fetchrow($select);
		
		//_pr($select,1);
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	
	
	
}
?>
