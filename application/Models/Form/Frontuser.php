<?php
//class Model_Form_Frontuser extends PS_Form
class Models_Form_Frontuser extends PS_Form
{	

	public function Listsearch()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$SearchTypeList = array(
			"user_name"=>'User Name',
			"email_address"=>'Email Address',						
			"user_types"=>'User Type',
		);
						
		$objTxtSearch = new Zend_Form_Element_Text('txtsearch');
		$objTxtSearch
			->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_FRONT_USER_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		$objSearchType = new Zend_Form_Element_Select('searchtype');
		$objSearchType
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($SearchTypeList);
			
		
		$objSearchButton = new Zend_Form_Element_Submit('search',$objTranslate->_('ADMIN_BUTTON_LABEL_FRONT_USER_SEARCH'));
		$objSearchButton
            ->setAttrib('id', 'search')
            ->setAttrib('class','btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		
		$this->addElements(array($objTxtSearch,$objSearchType,$objSearchButton));
		
	}
	
	
	public function Account()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		//$this->setMethod('post');
	   global $arrUserOption;

		$objUserOption = new Zend_Form_Element_Radio('user_option');
		$objUserOption
	        ->setRequired(true)
			->setAttrib('class','validate[required]')
			->setAttrib('id', 'optionsRadios1')						
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_ACCOUNT_OPTION'))))
			->addMultiOptions($arrUserOption)
			->setValue('1');																						

			
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('FRONT_BUTTON_LABEL_ACCOUNT_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
					

		$this->addElements(array($objUserOption,$objAddButton));
	}
	
	
	
	
}
?>
