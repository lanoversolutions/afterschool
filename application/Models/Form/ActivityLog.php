<?php
class Models_Form_ActivityLog extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		global $arrStatus;    
			    		
		$objProvider_Status = new Zend_Form_Element_Select('provider_status');
		$objProvider_Status
	        //->setRequired(true)
			->setAttrib('class','validate[required]')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_CATEGORY'))))
			->addMultiOptions($arrStatus)
			->setValue('0');
		
										
		
		/*$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_USER_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_USER_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');*/													
							
		
		$this->addElements(array( $objProvider_Status ,/*$objAddButton ,$objEditButton */));
				
	}	


}
?>
