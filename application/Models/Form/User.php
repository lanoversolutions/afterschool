<?php
class Models_Form_User extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		        		
		$objUsername = new Zend_Form_Element_Text('username');
		$objUsername
	        ->setRequired(true)
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_USERNAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		/*$objPassword  = new Zend_Form_Element_Text('password');
		$objPassword
	        ->setRequired(false)
			->setAttrib('class','form-textbox validate[required]')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');*/
		
		$objname   = new Zend_Form_Element_Text('name');
		$objname
	        ->setRequired(true)
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
		$objEmail   = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)			
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addValidator('EmailAddress',  TRUE  )    		
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
																						
		
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_USER_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_USER_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
							
		
		$this->addElements(array( $objUsername /*,$objPassword*/ ,$objname ,$objEmail ,$objAddButton ,$objEditButton ));
				
	}	


	public function Listsearch()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$SearchTypeList = array(
			"username"=>"Username",
			"name"=>"Name",
			"email"=>"Email" 										
		);
						
		$objTxtSearch = new Zend_Form_Element_Text('txtsearch');
		$objTxtSearch
			->setRequired(true)
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		$objSearchType = new Zend_Form_Element_Select('searchtype');
		$objSearchType
			->setAttrib('class','medium')			
			->addMultiOptions($SearchTypeList);
			
		
		$objSearchButton = new Zend_Form_Element_Submit('search',$objTranslate->_('ADMIN_BUTTON_LABEL_USER_SEARCH'));
		$objSearchButton
            ->setAttrib('id', 'search')
            ->setAttrib('class','btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		
		$this->addElements(array($objTxtSearch,$objSearchType,$objSearchButton));
		
	}
	
}
?>
