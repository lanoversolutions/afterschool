<?php
class Models_Form_ForgotPassword extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		        		
		$objEmail = new Zend_Form_Element_Text('user_email');
		$objEmail
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
																						
		
		$objAddButton = new Zend_Form_Element_Submit('submit',$objTranslate->_('ADMIN_BUTTON_LABEL_SEND'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		
		$this->addElements(array( $objEmail ,$objAddButton ));
				
	}	


	  public function Resetpassword()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$this->setMethod('post');

	
		$objRegPassword = new Zend_Form_Element_Password('password');
		$objRegPassword
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objRegConPassword = new Zend_Form_Element_Password('conform_password');
		$objRegConPassword
	        ->setRequired(true)		
	        ->setAttrib('class','form-textbox validate[required,equals[password]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		

		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP4_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

	
		$this->clearElements();	
		
		
		$this->addElements(array($objRegPassword,$objRegConPassword,$objAddButton));

	}



	
}
?>
