<?php
class Models_Form_Contact extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		        		
		$objName = new Zend_Form_Element_Text('name');
		$objName
	        ->setRequired(true)
			->setAttrib('class','tb7 medium validate[required]')
			->setAttrib('size','40px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		$objEmail = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)
	        ->setAttrib('class','tb7 medium validate[required,custom[email]]')
			->setAttrib('size','40px')
			->addValidator('EmailAddress',  TRUE  )
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
		$objSubject = new Zend_Form_Element_Text('subject');
		$objSubject
	        ->setRequired(true)			
			->setAttrib('class','tb7 medium validate[required]')
			->setAttrib('size','40px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_SUBJECT'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
																						
		$objMessage = new Zend_Form_Element_Textarea('message');
		$objMessage
	        ->setRequired(true)
			->setAttrib('class','tb7 form-textarea-large validate[required]')
			->setAttrib('cols','30')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_MESSAGE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		$publickey = "6LeYvwoAAAAAAKeVsPK9aakRQfybYOOz9YmyY5jH";
		$privatekey = "6LeYvwoAAAAAAHkMQuEfHY0gTo6Q3ohEsoS9IrI4";
		$recaptcha = new Zend_Service_ReCaptcha($publickey, $privatekey);
        $objcaptcha = new Zend_Form_Element_Captcha('challenge', 
        								array('label'   => 'Please type each of the following words into the box below to continue', 'captcha' => 'ReCaptcha',
                    							'captchaOptions' =>	array('captcha' => 'ReCaptcha', 'service' => $recaptcha, 'theme' => 'clean')));
		
		
				
		$objSubmit = new Zend_Form_Element_Submit('Submit',$objTranslate->_('ADMIN_BUTTON_LABEL_CONTACT_ADD'));
		$objSubmit
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'button')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		
		$this->addElements(array( $objName, $objcaptcha ,$objEmail ,$objSubject ,$objMessage ,$objSubmit));
				
	}	


	
}
?>
