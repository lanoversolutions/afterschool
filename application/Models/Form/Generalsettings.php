<?php

//class Models_Form_Generalsettings extends PS_Form
class Models_Form_Generalsettings extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		        		
		$objGeneral_settingSite_title = new Zend_Form_Element_Text('site_title');
		$objGeneral_settingSite_title
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_SITE_TITLE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		
		$objGeneral_settingEmail_id = new Zend_Form_Element_Text('email_id');
		$objGeneral_settingEmail_id
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_EMAIL_ID'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
			$objGeneral_settingPaging_variable = new Zend_Form_Element_Text('paging_variable');
		    $objGeneral_settingPaging_variable
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','12')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_PAGING_VARIABLE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		   $objGeneral_settingPage_in_group = new Zend_Form_Element_Text('page_in_group');
		   $objGeneral_settingPage_in_group
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','12')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_PAGE_IN_GROUP'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objGeneral_settingMessage1 = new Zend_Form_Element_Textarea('message1');
		$objGeneral_settingMessage1
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_LABEL_GENERALSETTINGS_MESSAGE1'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		$objGeneral_setting_Contact_no = new Zend_Form_Element_Text('contact_no');
		$objGeneral_setting_Contact_no
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_LABID'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objGeneral_setting_Contact_address = new Zend_Form_Element_Textarea('contact_address');
		$objGeneral_setting_Contact_address
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_GENERALSETTINGS_ADDRESS'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
			
		$objGeneral_settingMessage2 = new Zend_Form_Element_Textarea('message2');
		$objGeneral_settingMessage2
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_LABEL_GENERALSETTINGS_MESSAGE2'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
			$objGeneral_settingMessage3 = new Zend_Form_Element_Textarea('message3');
		$objGeneral_settingMessage3
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_LABEL_GENERALSETTINGS_MESSAGE3'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
								
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_GENERALSETTINGS_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_GENERALSETTINGS_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
							
		
		$this->addElements(array($objGeneral_settingSite_title ,$objGeneral_settingEmail_id, $objGeneral_setting_Contact_no, $objGeneral_setting_Contact_address, $objGeneral_settingPaging_variable,$objGeneral_settingPage_in_group,$objGeneral_settingMessage1 ,$objGeneral_settingMessage2 ,$objGeneral_settingMessage3 ,$objAddButton ,$objEditButton ));
				
	}	

}
?>