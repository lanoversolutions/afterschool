<?php

//class Models_Form_Provider extends PS_Form

class Models_Form_Provider extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        
		global $arrProviderStatus;
	    global $arrClaim;			
		
		$objProName = new Zend_Form_Element_Text('name');
		$objProName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			////->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			

		$objProAddress = new Zend_Form_Element_Text('address');
		$objProAddress
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('id','gadres')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_ADDRESS'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			

		$objProCity = new Zend_Form_Element_Text('city');
		$objProCity
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('id','gcity')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_CITY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objProState = new Zend_Form_Element_Select('state');
		$objProState
	       ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id','state')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_STATE'))));
			
		$objProZip = new Zend_Form_Element_Text('zip');
		$objProZip
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_ZIP'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
     /*	$objProCategory = new Zend_Form_Element_Select('category');
		$objProCategory
	        ->setRequired(true)
			->setAttrib('class','validate[required]')						
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_CATEGORY'))))
			->addMultiOptions($arrProviderCategory)
			->setValue('MARTIAL-ARTS'); */


		$objProNote = new Zend_Form_Element_Textarea('note');
		$objProNote
	        //->setRequired(true)
			->setAttrib('class','form-textarea-large')
			->setAttrib('cols','30')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_NOTE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProPhone = new Zend_Form_Element_Text('phone');
		$objProPhone
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_PHONE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProUrl = new Zend_Form_Element_Text('url');
		$objProUrl
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objCapacity = new Zend_Form_Element_Text('capacity');
		$objCapacity
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objAgeRange = new Zend_Form_Element_Text('age_range');
		$objAgeRange
	        //->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



    	$objStatus = new Zend_Form_Element_Select('status');
		$objStatus
	        //->setRequired(true)
			->setAttrib('class','form-selectbox')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_CATEGORY'))))
			->addMultiOptions($arrProviderStatus)
			->setValue('MARTIAL-ARTS');


		$objIntroduction = new Zend_Form_Element_Textarea('introduction');
		$objIntroduction
	        //->setRequired(true)
			->setAttrib('class','form-textarea-large')
			->setAttrib('cols','30')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_DETAILS'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objWebsite = new Zend_Form_Element_Text('website');
		$objWebsite
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objLocation = new Zend_Form_Element_Text('location');
		$objLocation
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
	

		$objUrl = new Zend_Form_Element_Text('url');
		$objUrl
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		
		
		$objImagename = new Zend_Form_Element_File('imagename[]');
		$objImagename	
		//->setRequired(true)		
	    ->setAttrib('class', 'code')
		->setAttrib('id', 'customFieldName')
		->addFilter('StripTags')
		->addFilter('StringTrim')
		->setMaxFileSize(5242880) // 5mb
        ->addValidator('IsImage')
        ->addValidator('Count', false, 1)
        ->addValidator('Size', false, 5242880)
		->addValidator('Extension', false, array('jpg', 'jpeg', 'png', 'gif'))
		//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP4_URL'))))
		->removeDecorator('DtDdWrapper')
		->removeDecorator('Label')
		->addDecorator('PSWrapper');
		
		/*$objProFilename = new Zend_Form_Element_Text('filename');
		$objProFilename
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_FILENAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');*/


		$objProEmail = new Zend_Form_Element_Text('email');
		$objProEmail
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProDetails = new Zend_Form_Element_Textarea('details');
		$objProDetails
	        //->setRequired(true)
			->setAttrib('class','form-textarea-large')
			->setAttrib('cols','30')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_DETAILS'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objProKids_program_url = new Zend_Form_Element_Text('kids_program_url');
		$objProKids_program_url
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_KIDS_PROGRAM_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProSummer_program_url = new Zend_Form_Element_Text('summer_program_url');
		$objProSummer_program_url
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_SUMMER_PROGRAM_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProVideo_url = new Zend_Form_Element_Text('video_url');
		$objProVideo_url
	        //->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			//->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_VIDEO_URL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objProClaim = new Zend_Form_Element_Radio('claim');
		$objProClaim
	        //->setRequired(true)
			->setAttrib('class','validate[required]')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_PROVIDER_CATEGORY'))))
			->addMultiOptions($arrClaim)
			->setValue('0');
		
					
		$objAddButton = new Zend_Form_Element_Button('add_btn',$objTranslate->_('FRONT_BUTTON_LABEL_PROVIDER_ADD'));
		$objAddButton
	        //->setRequired(true)		
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success btngeocode')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		$objEditButton = new Zend_Form_Element_Button('edit_btn',$objTranslate->_('FRONT_BUTTON_LABEL_PROVIDER_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success btngeocode')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
							
		
		$this->addElements(array( $objProName, $objProAddress, $objProCity, $objProState, $objProZip, /*$objProCategory, */ $objProNote , $objProPhone , $objImagename , $objProUrl ,/*$objProFilename , */$objCapacity ,$objAgeRange , $objIntroduction ,$objStatus ,$objLocation ,$objUrl ,$objWebsite,$objProEmail ,$objProDetails ,$objProKids_program_url ,$objProSummer_program_url ,$objProVideo_url,$objProClaim ,$objAddButton, $objEditButton));
				
	}	

}
?>
