<?php

//class Models_Form_Review extends PS_Form

class Models_Form_Review extends PS_Form
{	
	
	
	public function __construct($id = null, $table = 'afterschool')
	{
	    parent::__construct($id);
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        $this->setName('review');
		global $arrRating_Type;		
		 		        		
		$objName = new Zend_Form_Element_Text('name');
		$objName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			//->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		$objEmail = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)
	        ->setAttrib('class','form-textbox validate[required,custom[email]]')
			//->setAttrib('size','50px')
			->addValidator('EmailAddress',  TRUE  )
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
		$objRating = new Zend_Form_Element_Select('rating');
		$objRating
	        ->setRequired(true)		
			->setAttrib('class','form-selectbox validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REVIEW_RATING'))))			
			->addMultiOptions($arrRating_Type);
																						
		$objMessage = new Zend_Form_Element_Textarea('comments');
		$objMessage
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('cols','15')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REVIEW_MESSAGE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
/*		$publickey = "6LeYvwoAAAAAAKeVsPK9aakRQfybYOOz9YmyY5jH";
		$privatekey = "6LeYvwoAAAAAAHkMQuEfHY0gTo6Q3ohEsoS9IrI4";
		$recaptcha = new Zend_Service_ReCaptcha($publickey, $privatekey);
        $objcaptcha = new Zend_Form_Element_Captcha('challenge', 
        								array('label'   => 'Please type each of the following words into the box below to continue', 'captcha' => 'ReCaptcha',
                    							'captchaOptions' =>	array('captcha' => 'ReCaptcha', 'service' => $recaptcha, 'theme' => 'clean')));
*/		

	    $objhiddenPid = new Zend_Form_Element_Hidden('id');
        $objhiddenPid->setValue($id)
        		->removeDecorator('label')
              	->removeDecorator('HtmlTag');
		
        $objhiddenType = new Zend_Form_Element_Hidden('type');
        $objhiddenType->setValue($table)
        		->removeDecorator('label')
              	->removeDecorator('HtmlTag');
				
		$objSubmit = new Zend_Form_Element_Submit('Submit',$objTranslate->_('FRONT_BUTTON_LABEL_REVIEW_ADD'));
		$objSubmit
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		
		$this->addElements(array( $objName, /*$objcaptcha,*/ $objEmail ,$objRating ,$objhiddenPid ,$objhiddenType ,$objMessage ,$objSubmit));
		
		
		//$this->setAction('/review/new');
		$this->setMethod('post');
		
				
	}	


	
}
?>
