<?php

//class Models_Form_Register extends PS_Form

class Models_Form_Register extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        
		global $arrUserOption;
				
		$objRegFirstName = new Zend_Form_Element_Text('first_name');
		$objRegFirstName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->setAttrib('maxlength','20')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
			
		$objRegLastName = new Zend_Form_Element_Text('last_name');
		$objRegLastName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_LAST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		
		$objRegEmail = new Zend_Form_Element_Text('email');
		$objRegEmail
			->setRequired(true)		 
	        ->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('EmailAddress')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');		
			
		
		$objRegConEmail = new Zend_Form_Element_Text('conform_email');
		$objRegConEmail
			->setRequired(true)		
	        ->setAttrib('class','form-textbox validate[required,custom[email],equals[email]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('EmailAddress')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');		

		
			
		$objRegPassword = new Zend_Form_Element_Password('password');
		$objRegPassword
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objRegConPassword = new Zend_Form_Element_Password('conform_password');
		$objRegConPassword
	        ->setRequired(true)		
	        ->setAttrib('class','form-textbox validate[required,equals[password]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objUserOption = new Zend_Form_Element_Select('user_option');
		$objUserOption
	        ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')						
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('FRONT_MSG_INVALID_ACCOUNT_OPTION'))))
			->addMultiOptions($arrUserOption)
			->setValue('1');																						

					
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_REGISTRATIONUSER_ADD'));
		$objAddButton
	        ->setRequired(true)		
            ->setAttrib('id', 'add_btn validate[required]')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_REGISTRATIONUSER_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
							
		
		$this->addElements(array( $objRegFirstName, $objRegLastName, $objRegEmail, $objRegConEmail,$objRegPassword,$objRegConPassword,$objUserOption ,$objAddButton, $objEditButton));
				
	}	


//userprofile form
	
  public function Userprofile()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$this->setMethod('post');

	
		/*$objRegFirstName = new Zend_Form_Element_Text('first_name');
		$objRegFirstName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
			
		$objRegLastName = new Zend_Form_Element_Text('last_name');
		$objRegLastName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_LAST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');*/
		

		$objRegPassword = new Zend_Form_Element_Password('password');
		$objRegPassword
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->setAttrib('placeholder','Please enter your old password')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_OLD_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objRegNewPassword = new Zend_Form_Element_Password('newpassword');
		$objRegNewPassword
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->setAttrib('placeholder','Please enter your new password')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_NEW_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objRegReTypePassword = new Zend_Form_Element_Password('retype_password');
		$objRegReTypePassword
	        ->setRequired(true)		
	        ->setAttrib('class','form-textbox validate[required,equals[newpassword]]')
			->setAttrib('size','50px')
			->setAttrib('placeholder','Please enter retype password')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_REGISTRATIONUSER_RETYPE_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


	  

		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_CHANGE_PASSWORD'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													


	
		$this->clearElements();	
		
		
		$this->addElements(array(/*$objRegFirstName,$objRegLastName,*/$objRegPassword,$objRegNewPassword,$objRegReTypePassword,$objEditButton));

	}



}
?>
