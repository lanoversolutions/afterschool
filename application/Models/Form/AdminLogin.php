<?php
//class Model_Form_AdminLogin extends PS_Form
class Models_Form_AdminLogin extends PS_Form
{

	public function init()
	{
	    parent::init();
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);

		$this->setMethod('post');

		$objUsername = new Zend_Form_Element_Text('username');
		$objUsername
			->setRequired(true)
			->setAttrib('class','input-large span10')
			->setAttrib('placeholder','type username')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('EMPTY_USERNAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objPassword = new Zend_Form_Element_Password('password');
		$objPassword
			->setRequired(true)
			->setAttrib('class','input-large span10')
			->setAttrib('placeholder','type password')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('EMPTY_PASSWORD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		//$objSubmit = new Zend_Form_Element_Submit('submit', $objTranslate->_('ADMIN_FORM_LABEL_LOGIN_LOGINBUTTON'));
		$objSubmit = new Zend_Form_Element_Button('submit', $objTranslate->_('ADMIN_FORM_LABEL_LOGIN_LOGINBUTTON'));
		$objSubmit
        	->setAttrib('type', 'submit')    
			->setAttrib('id', 'submitbutton')
            ->setAttrib('value', $objTranslate->_('ADMIN_FORM_LABEL_LOGIN'))
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
					

		$this->addElements(array($objUsername,$objPassword,$objSubmit));
	}
}
