<?php

/**
 * Class for front_user table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Frontuser extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'front_user'; 		
                   
	/**
     * Get all front_user listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='')
	{ 	 										    	    	    	   		 
	 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "f.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('f'=>'front_user'),array('f.user_id','f.*'))
				->where('f.user_option = ?',1);
		
	
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('f.user_id ASC');
		else
			$select->order($strSort);
	
	 return $select;	 	    				
  	}
  



	/**
     * Get all front_user listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function gettenantList($searchText='',$searchType='',$sortby='')
	{ 	 										    	    	    	   		 
	 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "f.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('f'=>'front_user'),array('f.user_id','f.*'))
				->where('f.user_option = ?',2);
		
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('f.user_id ASC');
		else
			$select->order($strSort);
	
	 return $select;	 	    				
  	}
  	
	 /**
     * Fetch Particular front_user Detail
     *	 
     */
    /*----------------------| Get spacific front_user Detail   |----------------------*/
    public function fetchEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.*'))								
				//->where('u.is_admin != 1')
				->where('f.user_id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }
	
	
		 /**
     * Fetch Particular front_user Detail
     *	 
     */
    /*----------------------| Get spacific front_user Detail   |----------------------*/
    public function fetchuserdetails($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.user_id','f.first_name','f.last_name'))								
				//->where('u.is_admin != 1')
				->where('f.user_id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }

	
	
	  	 /**
     * Fetch Particular password Detail
     *	 
     */
    /*----------------------| Get spacific email Detail   |----------------------*/
	
	public function fetchpassword($password)
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.password'))								
				->where('f.password = ?', $password);
				
		$select = $this->fetchRow($select);
		
		if ($select){
				return $select = $select->toArray(); 
		}else{
			return null;
	   }
	}

	
	 /**
     * Fetch Particular front_user Detail
     *	 
     */
    /*----------------------| Get spacific front_user Detail   |----------------------*/
    public function fetchaccountEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.*'))								
				//->where('u.is_admin != 1')
				->where('f.user_id = ?',$id);										
    	
    	$select = $this->fetchRow($select);							
				
		if($select)
	    	return $select = $select->toArray(); 
		else
      		return null;    
    }
	

	
	    /**
     * Update data for front_user entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         

	
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "user_id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
	
    
		/**
     * Delete data forom front_user entry.
     *     
     */         
	/*----------------------| delete data  |-----------------------------*/   
  	public function deleteData($id) {
    	$objData = $this->fetchRow('user_id='.$id);	
		if(!empty($objData)){
    		$objData->delete();
    	}	
    	unset($objData);
  	}

/*----------------------| countlandlords data  |-----------------------------*/   
	  public function countactivity() {
	
      $objTranslate = Zend_Registry::get('Zend_Translate');
	  $select = $this->select();
	  $select->setIntegrityCheck(false)
			->from(array('a'=>'activity'),array('a.id','a.*'));
	
	  $select = $this->fetchAll($select);
	  $select = $select->toArray();   
	  $no =count($select);
	  return $no;
	 
		}


/*----------------------| counttenant data  |-----------------------------*/   
	  public function countafterschool() {
	
      $objTranslate = Zend_Registry::get('Zend_Translate');
	  $select = $this->select();
	  $select->setIntegrityCheck(false)
			->from(array('as'=>'afterschool'),array('as.id','as.*'));
	
	  $select = $this->fetchAll($select);
	  $select = $select->toArray();   
	  $no =count($select);
	  return $no;
		}


	    /**
     * Update data for front_user entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         

	
	/*----------------------| update data  |-----------------------------*/   
	public function updateaccountData(array $data,$id) { 
		$where = "user_id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
	


}
?>
