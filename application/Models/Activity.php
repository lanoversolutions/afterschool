<?php

/**
 * Class for Activity table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Activity extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'activity'; 		
                   


  	/**
     * Get all activity and activity_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getDataList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 //$cond_string = "a.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select1 = $this->select();
	 $select1->setIntegrityCheck(false)	 			
				->from(array('a'=>'activity'),array('a.id','a.name','a.address','a.email','a.category'))
				->where('a.user_id = '.$user_id)
				->where('a.log_status = ?','1');
	
	$select2 = $this->select();
	$select2->setIntegrityCheck(false)	 			
				->from(array('af'=>'afterschool'),array('af.id','af.name','af.address','af.email','af.category'))
				->where('af.user_id = '.$user_id)
				->where('af.log_status = ?','1');
	
	$select = $this->select()->union(array($select1, $select2), Zend_Db_Select::SQL_UNION_ALL)
				->order('id ASC');
		
					 	 	 				 
	if($search_txt != '' && $search_type != '')
		$select->where($cond_string);
	 
	if(trim($sortby)=='');
	
	else
		$select->order($strSort);
	
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
 
	


	 /**
     * Fetch activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchdetails() {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.details <> ?','')
				->where('a.category = ?','MARTIAL-ARTS')
				->where('a.kids_program_url <> ?','')
				->where('a.approved = ?',1);

    	$select->order('a.updated DESC')
			    ->limit(5);
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }


	 /**
     * Fetch activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchyouthsports() {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.details <> ?','')
				->where('a.category = ?','YOUTH-SPORTS')
				->where('a.approved = ?',1);

    	$select->order('a.updated DESC')
			   ->limit(5);
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	 /**
     * Fetch  activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchMartialArts($state_code) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.state = ?',$state_code)
				->where('a.category = ?','MARTIAL-ARTS')
				->where('a.kids_program_url <> ?','')
				->where('a.approved = ?',1);

    	$select->order('a.updated desc')
			   ->limit(5);
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	 /**
     * Fetch activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchYouthSportsData($state_code) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.state = ?',$state_code)
				->where('a.category = ?','YOUTH-SPORTS')
				->where('a.details <> ?','')
				->where('a.approved = ?',1);

    	$select->order('a.updated desc')
			   ->limit(5);
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	 /**
     * Fetch activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchMartialArtsDetails($state_code,$cityname) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))
				->where('a.category = ?','MARTIAL-ARTS')
				->where('a.state = ?',$state_code)
				->where('a.city = ?',$cityname)
				->where('a.approved = ?',1);

    	$select->order('a.name asc');
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	 /**
     * Fetch activity Detail
     *	 
     */
    /*----------------------| Get activity Detail   |----------------------*/
    public function fetchYouthSportsDetails($state_code,$cityname) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))
				->where('a.category = ?','YOUTH-SPORTS')
				->where('a.state = ?',$state_code)
				->where('a.city = ?',$cityname)
				->where('a.approved = ?',1);

    	$select->order('a.name asc');
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }




	 /**
     * fetch Particular activity Detail
     *	 
     */
    /*----------------------| Get spacific activity Detail   |----------------------*/
    public function fetchdata($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }




	 /**
     * fetch Particular activity Detail
     *	 
     */
    /*----------------------| Get spacific activity Detail   |----------------------*/
    public function fetchactivitydetails($activity_id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity'),array('a.*'))								
				->where('a.id = ?',$activity_id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }



   	/**
     * Get all activity_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "al.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('al'=>'activity_log'),array('al.*'));
				
					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('al.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}



   	/**
     * Get all activity_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getactivityList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "al.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('al'=>'activity_log'),array('al.*'))
				->where('al.provider_status = ?', '0');
				
					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('al.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}


	/**
    * Save data for new activity entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
		
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }



    /**
     * Update data for activity entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }




    /**
     * Update data for activity entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateactivityData(array $data,$activity_id) { 
		$where = "id = " . $activity_id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }



}
?>
