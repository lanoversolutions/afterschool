<?php

/**
 * Class for Cities table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Cities extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'cities'; 		
                   

	
	 /**
     * Fetch All cities
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchcity($state_code) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('c.state = ?',$state_code)
				->where('c.afterschool_count >= ? ', 1);
				
		$select->order('c.city asc');
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	

	 /**
     * Fetch All cities
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchMartialArtsCity($state_code) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('c.state = ?',$state_code)
				->where('c.activities_count >= ? ', 1);
				
		$select->order('c.city asc');
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	 /**
     * Fetch All cities
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchcityname($filename) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('c.filename = ?',$filename);
				
		//echo $select; exit;
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	
	 /**
     * Fetch All cities 
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchSportsCount($state_code) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('c.state = ?',$state_code)
				->where('c.sports_count >= ? ', 1);
		
		$select->order('c.city asc');		
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	 /**
     * Fetch All cities
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchcitylisting($cityname,$statecode) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('city = ?', $cityname)
				->where('state = ?' , $statecode);
			
		//echo $select; exit;
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }



	 /**
     * Fetch All cities
     *	 
     */
    /*----------------------| Get All cities Detail   |----------------------*/
    public function fetchCityEntry($city,$state) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'cities'),array('c.*'))								
				->where('c.city = ?',$city)
				->where('c.state = ?',$state);
				
		//echo $select; exit;
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }




    /**
     * Update data for cities entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updatCount(array $data,$cityId) { 
		$where = "id = " . $cityId;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }


			 	      	
}
?>
