<?php

/**
 * Class for GeneralSettings table related database operations.
 *
 * @category   PS
 * @package    Models_Pages
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Generalsettings extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'general_setting'; 		
                   
	/**
     * Get all GeneralSettings listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	 
	 
	 public function getGeneralSettings()
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('g'=>'general_setting'),array('g.*','g.site_title as SITE_TITLE','g.email_id as EMAIL_ID','g.paging_variable as PAGING_VARIABLE','g.page_in_group as TOTAL_PAGE_IN_GROUP'));										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();  
		return $select ;
	}	 
	 
  	
	 /**
     * Fetch Particular GeneralSettings Detail
     *	 
     */
    /*----------------------| Get spacific GeneralSettings Detail   |----------------------*/
    public function fetchEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('g'=>'general_setting'),array('g.*'))								
				->where('g.id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;
    
    }
	
	
	/**
    * Save data for new GeneralSettings entry.
    *
    * @param  array $data Array of GeneralSettings data to insert in database.
    * @return integer|boolean Last inserted page id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }                        	
        $data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');   
		
        return $this->insert($data);
     }
			 	     

    /**
     * Update data for GeneralSettings entry.
     *
     * @param  array $data Array of GeneralSettings data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted GeneralSettings id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}												
		$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
    
	
}
?>
