<?php

/**
 * Class for ActivityLog table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_ActivityLog extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'activity_log'; 		



	 	/**
     * Make subject Combobox
     *	 
     */
    /*----------------------| Get States Combobox   |----------------------*/
    public function getStateCombobox() {
		
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.id','s.state_name','s.state_code'))
				->order('s.state_name');												
    	
    	$select = $this->fetchAll($select);
		$select = $select->toArray();				
		
		$arrStates = array();
		$arrStates[''] = $objTranslate->translate('ADMIN_LABEL_STATES_SELECT');
		foreach($select as $row){
			$arrStates[$row['state_code']] = $row['state_name'];
		}		
		//_pr($arrStates,1);
		if($arrStates)
	    	return $arrStates;
		else
      		return null;    
    }
                   

	 /**
     * fetch Particular activity_log Detail
     *	 
     */
    /*----------------------| Get spacific activity_log Detail   |----------------------*/
    public function fetchdetails($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('al'=>'activity_log'),array('al.*'))								
				->where('al.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	 /**
     * fetch Particular activity_log Detail
     *	 
     */
    /*----------------------| Get spacific activity_log Detail   |----------------------*/
    public function fetchExistRecord($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('al'=>'activity_log'),array('al.*'))								
				->where('al.activity_id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }



  	/**
     * Get all activity and activity_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 //$cond_string = "a.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select1 = $this->select();
	 $select1->setIntegrityCheck(false)	 			
				->from(array('a'=>'activity'),array('a.id','a.name','a.address','a.email','a.category'))
				->where('a.user_id = '.$user_id);
	
	$select2 = $this->select();
	$select2->setIntegrityCheck(false)	 			
				->from(array('af'=>'afterschool'),array('af.id','af.name','af.address','af.email','af.category'))
				->where('af.user_id = '.$user_id);
	
	$select = $this->select()->union(array($select1, $select2), Zend_Db_Select::SQL_UNION_ALL)
				->order('id ASC');
		
					 	 	 				 
	if($search_txt != '' && $search_type != '')
		$select->where($cond_string);
	 
	if(trim($sortby)=='');
	
	else
		$select->order($strSort);
	
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
 



  	/**
     * Get all afterschool_log and  activity_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getpendingList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 //$cond_string = "a.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select1 = $this->select();
	 $select1->setIntegrityCheck(false)	 			
				->from(array('a'=>'activity_log'),array('a.id','a.name','a.address','a.email','a.category','a.provider_status'))
				->where('a.provider_status <> ?','1')
				->where('a.user_id = '.$user_id);
	
	$select2 = $this->select();
	$select2->setIntegrityCheck(false)	 			
				->from(array('af'=>'afterschool_log'),array('af.id','af.name','af.address','af.email','af.category','af.provider_status'))
				->where('af.provider_status <> ?','1')
				->where('af.user_id = '.$user_id);
	
	$select = $this->select()->union(array($select1, $select2), Zend_Db_Select::SQL_UNION_ALL)
				->order('id ASC');
		
					 	 	 				 
	if($search_txt != '' && $search_type != '')
		$select->where($cond_string);
	 
	if(trim($sortby)=='');
	
	else
		$select->order($strSort);
	
	 //echo $select;exit;	 	 			
	 $select = $this->fetchAll($select);
	 
	if($select)
			return $select = $select->toArray();
			
		else
      		return null;	 	    				
  	}
 



	/**
    * Save data for new activity_log entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
		
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		if(isset($data['id']) && $data['id'] == '')
			unset($data['id']);
		
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        
		
		 // _pr($data,1);
        return $this->insert($data);
     }


    /**
     * Update data for activity_log entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "activity_id  = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
		unset($data['id']);
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }



    /**
     * Update data for activity_log entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updatStatus(array $data,$id) { 
		$where = "id = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
		unset($data['id']);
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }

			 	     
	/**
     * Delete data forom activity_log entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
  	public function deleteData($id) {
    	$objData = $this->fetchRow('id='.$id);	
		if(!empty($objData)){
    		$objData->delete();
    	}	
    	unset($objData);
  	}



}
?>
