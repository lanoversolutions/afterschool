<?php

/**
 * Class for AfterschoolLog table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_AfterschoolLog extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'afterschool_log'; 		




   	/**
     * Get all afterschool_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "al.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('al'=>'afterschool_log'),array('al.*'))
				->where('al.provider_status = ?', '0');
				
					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('al.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}



	 /**
     * fetch Particular afterschool_log Detail
     *	 
     */
    /*----------------------| Get spacific afterschool_log Detail   |----------------------*/
    public function fetchdetails($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('al'=>'afterschool_log'),array('al.*'))								
				->where('al.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	 /**
     * fetch Particular afterschool_log Detail
     *	 
     */
    /*----------------------| Get spacific afterschool_log Detail   |----------------------*/
    public function fetchAfterschooldetails($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('al'=>'afterschool_log'),array('al.*'))								
				->where('al.afterschool_id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }



	 /**
     * fetch Particular afterschool_log Detail
     *	 
     */
    /*----------------------| Get spacific afterschool_log Detail   |----------------------*/
    public function fetchdata($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	/**
    * Save data for new afterschool_log entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
		
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
		
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
				if(isset($data['id']) && $data['id'] == '')
			unset($data['id']);

		
		//$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['created_date'] = date('Y-m-d H:i:s');                        
        //_pr($data,1);
		
        return $this->insert($data);
     }
			 	     

    /**
     * Update data for afterschool_log entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }



    /**
     * Update data for afterschool_log entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateAllData(array $data,$id) { 
		$where = "afterschool_id  = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
		//_pr($data,1);
		unset($data['id']);
								
		return $this->update($data,$where);
    }





    /**
     * Update data for afterschool_log entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updatStatus(array $data,$id) { 
		$where = "id = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
		unset($data['id']);
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }






}
?>
