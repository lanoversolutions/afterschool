<?php
class Models_AdminLogin extends Zend_Db_Table_Abstract
{
    protected $_name    = 'user';
    protected $_adapter;
    protected $_auth; 

    public function verifyLoginInfo($data) {
        
    	$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);

    	$dbAdapter = Zend_Registry::get(PS_App_DbAdapter);

        $this->_adapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $this->_adapter->setTableName('user');
        $this->_adapter->setIdentityColumn('username');
        $this->_adapter->setCredentialColumn('password');
        $this->_adapter->setCredentialTreatment('SHA1(?)');
		        
        //set the formData        
        $this->_adapter->setIdentity($data['username']);
        $this->_adapter->setCredential($data['password']);

        // get Zend_Auth object
        $this->_auth = Zend_Auth::getInstance();
		
        //now authenticate it
        $result = $this->_auth->authenticate($this->_adapter);
        if($result->isValid()) {
            return true;
        }

        return false;
    }

    public function setSession() {       			
		
		$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);

        $objSess = new Zend_Session_Namespace(PS_App_Auth);
        $storage = new Zend_Auth_Storage_Session();

        $arrData = array('id','username','name','email','is_admin');		
        $objRow = $this->_adapter->getResultRowObject($arrData);		
        $storage->write($objRow);
		
        $objUser = $this->_auth->getIdentity();		
		//_pr($objUser,1);        
		$objSess->user_id = $objUser->id;
        $objSess->username = $objUser->username;
		$objSess->name = $objUser->name;
        $objSess->email = $objUser->email;			
		$objSess->is_admin = $objUser->is_admin;								
		$objSess->isAdminLogin  = true;
		     
        //$objError->message = $objTranslate->_("LOGIN_SUCCESS");
        $objError->message = $objTranslate->translate('LOGIN_SUCCESS');
    }

    public function clearSession() {
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
	
        Zend_Auth::getInstance()->clearIdentity();

	    $objSess = new Zend_Session_Namespace(PS_App_Auth);
		$objSess->user_id       = "";
		$objSess->username   	= "";
		$objSess->name 	 		= "";
		$objSess->email 	 	= "";
		$objSess->is_admin 	 	= "";		
		$objSess->isAdminLogin  = false;
		$objSess->__unset(PS_App_Auth);
		$objError = new Zend_Session_Namespace(PS_App_Error);
		$objError->message = $objTranslate->translate('LOGOUT_SUCCESS');
		$objError->messageType = 'confirm';
    }

    public function save(array $data)
    {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        $data['date_created'] = date('Y-m-d H:i:s');
        return $this->insert($data);
    }

    public function fetchEntries()
    {
        return $this->fetchAll('1')->toArray();
    }

    public function fetchEntry($id)
    {
        $select = $this->select()->where('user_master_id = ?', $id);
        return $this->fetchRow($select)->toArray();
    }
     public function checkemail($email)
    {
    	//echo "Hello";exit;
    	$select = $this->select();
		$select->where("email = '".$email."' AND status = 'Yes'");
		//echo $select;exit;
		if( $this->fetchRow($select) )
			return $this->fetchRow($select)->toArray();
		else
			return false;
    }
	
		
}


