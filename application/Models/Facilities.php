<?php

/**
 * Class for Facilities table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Facilities extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'afterschool'; 		
                   

	
	 /**
     * Fetch All afterschool
     *	 
     */
    /*----------------------| Get All afterschool Detail   |----------------------*/
    public function fetchfacilitiesData($state_code,$cityname) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.state = ?',$state_code)
				->where('a.city = ?', $cityname)
				->where('a.approved = ? ', 1);
				
		$select->order("name asc");
		$select->order("location asc");
		$select->order("address asc");
		
		//echo $select; exit;
		
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	 /**
     * Fetch All afterschool
     *	 
     */
    /*----------------------| Get All afterschool Detail   |----------------------*/
    public function fetchdata($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }

	
}
?>
