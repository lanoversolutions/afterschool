<?php

/**
 * Class for Reviews table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Reviews extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'reviews'; 		
                   


	/**
     * Get all Reviews listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getreviewList($searchText='',$searchType='',$sortby='')
	{ 	 										    	    	    	   		 
	 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "r.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('r'=>'reviews'),array('r.*'))
				->where('r.approved = ?','0');

		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('r.id DESC');
		else
			$select->order($strSort);
	
		//echo $select; exit;
	 return $select;	 	    				
  	}
  


	/**
     * Get all Reviews listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getreviewsList($searchText='',$searchType='',$sortby='',$user_id = '')
	{ 	 										    	    	    	   		 
	 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "r.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('r'=>'reviews'),array('r.*'))
				->where('r.user_id = ?',$user_id);

		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('r.id DESC');
		else
			$select->order($strSort);
	
		//echo $select; exit;
	 return $select;	 	    				
  	}



	/**
     * Get all Reviews listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all Reviews data |---------------*/		
    public function getApprovedReviewList($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
	    $select->setIntegrityCheck(false)	 			
				->from(array('r'=>'reviews'),array('r.*','DATE_FORMAT(r.review_date,"%h:%i %p on %b %d %Y") AS date'))
				->where('r.approved = ?','1')
				->where('r.table = ?',"afterschool")
				->where('r.provider_id = ?',$id);

    	$select->order('r.id DESC');
	   //echo $select; exit;
	   	
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	/**
     * Get all Reviews listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all Reviews data  |---------------*/		
    public function getActivityReviewList($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
	    $select->setIntegrityCheck(false)	 			
				->from(array('r'=>'reviews'),array('r.*','DATE_FORMAT(r.review_date,"%h:%i %p on %b %d %Y") AS date'))
				->where('r.approved = ?','1')
				->where('r.table = ?',"activity")
				->where('r.provider_id = ?',$id);

    	$select->order('r.id DESC');
	   //echo $select; exit;
	   	
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	 /**
     * Fetch Particular reviews Detail
     *	 
     */
    /*----------------------| fetch all Reviews data   |----------------------*/
    public function fetchMartialArtsReviews($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('r'=>'reviews'),array('r.*'))
				->where('r.provider_id = ?',$id)
				->where('r.table = ?',"activity")
				->where('r.approved = ?','1');

    	
	   //echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }


	 /**
     * Fetch Particular reviews Detail
     *	 
     */
    /*----------------------| Get spacific reviews Detail   |----------------------*/
    public function fetchdetails($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('r'=>'reviews'),array('r.*'))
				->where('r.id = ?', $id);
    	
		//echo $select; exit;
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }


	 /**
     * Fetch Particular reviews Detail
     *	 
     */
    /*----------------------| Get spacific reviews Detail   |----------------------*/
    public function fetchReviewsData($id,$email) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('r'=>'reviews'),array('r.*'))
				->where('r.id = ?', $id)
				->where('r.email = ?', $email);
    	
		//echo $select; exit;
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



	    /**
     * Update data for reviews entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         

	
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id ,$provider_id) { 
		$where = "id = " . $id ." AND provider_id = ".$provider_id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		//_pr($where,1);															
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }




	/**
    * Save data for new reviews entry.
    *
    * @param  array $data Array of reviews data to insert in database.
    * @return integer|boolean Last inserted author id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		unset($data["id"]);
		//_pr($data,1);
		/*$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        */
        
        return $this->insert($data);
     }


}
?>
