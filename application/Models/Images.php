<?php

/**
 * Class for Images table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Images extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'images'; 		
                   



	 /**
     * Fetch Particular images Detail
     *	 
     */
    /*----------------------| Get spacific images Detail   |----------------------*/
    public function fetchMartialArtsImages($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('i'=>'images'),array('i.*'))
				->where('i.provider_id = ?',$id)
				//->where('i.type = ? ',"activity")
				->where('i.approved = ? ',1 );
				//->where('i.approved = ? ',-1 );

    	
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }



		 /**
		 * Fetch Particular images Detail
		 *	 
		 */
		/*----------------------| Get spacific images Detail   |----------------------*/
		public function fetchactivityImages($id,$image_type) {
			$objTranslate = Zend_Registry::get('Zend_Translate');
			$select = $this->select();
			$select->setIntegrityCheck(false)
					->from(array('i'=>'images'),array('i.*'))
					->where('i.provider_id = ?',$id)
					//->where('i.type = ? ',"activity")
					->where('i.type = ? ',$image_type)
					->where('i.approved = ? ',1 );
					//->where('i.approved = ? ',-1 );
	
			
			//echo $select; exit;
			$select = $this->fetchAll($select);
			
			if($select)
				return $select = $select->toArray();
								
			else
				return null;    
		}
	



	 /**
     * Fetch Particular images Detail
     *	 
     */
    /*----------------------| Get spacific images Detail   |----------------------*/
    public function fetchlogImages($log_id ,$image_type) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('i'=>'images'),array('i.*'))
				->where('i.provider_id = ?',$log_id)
				->where('i.type = ? ',$image_type);
				

    	
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }

	 /**
     * Fetch Particular images Detail
     *	 
     */
    /*----------------------| Get spacific images Detail   |----------------------*/
    public function fetchImages($id,$image_type) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('i'=>'images'),array('i.*'))
				->where('i.type = ? ',$image_type)
				->where('i.provider_id = ?',$id);

		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
							
		else
      		return null;    
    }





		 /**
     * Fetch Particular images Detail
     *	 
     */
    /*----------------------| Get spacific images Detail   |----------------------*/
    public function fetchimage($id) {
	
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('i'=>'images'),array('i.*'))
				->where('i.id = ?',$id);																			

		
		//echo $select;exit;		    	
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }



	/**
    * Save data for new images entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
		
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		//_pr($data,1);
		//$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['created'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }



    /**
     * Update data for images entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateImages (array $data,$image_type,$id) { 
		$where = "provider_id=".$id." AND type='".$image_type."'";
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }


    /**
     * Update data for images entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateImagesstatus (array $data,$id) { 
		$where = "id = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }

/**
     * Delete data forom images entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteimages($id) {									
		$objData = $this->fetchRow('id ='.$id);
			
		if($objData)
			$objData->delete();
		else
      		return null;    
    
		unset($objData);
  	}



}
?>
