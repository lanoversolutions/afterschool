<?php

/**
 * Class for user_log table related database operations.
 *
 * @category   PS
 * @package    Models_UserLog
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_UserLog extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'user_log'; 		
                   
	/**
     * Get all user_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='')
	{ 
	 
	 switch($sortby){
		case 'A1':
				$strSort = "user ASC";
			 break;
		case 'D1':
				$strSort = "user DESC";
			 break;
		case 'A2':
				$strSort = "lead ASC";
			 break;
		case 'D2':
				$strSort = "lead DESC";
			 break;	
		case 'A3':
				$strSort = "user_operation ASC";
			 break;
		case 'D3':
				$strSort = "user_operation DESC";
			break;
		case 'A4':
				$strSort = "dcreated ASC";
			 break;
		case 'D4':
				$strSort = "dcreated DESC";
			break;														 	 
		default:			
				$strSort = "id DESC";
			break;
	 }
	 
	 
	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "ul.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('ul'=>'user_log'),array('ul.id','ul.user','ul.lead','ul.user_operation','DATE_FORMAT(ul.dcreated,"%m/%d/%Y %h:%i %p") AS dcreated'));				
							 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('ul.id ASC');
		else
			$select->order($strSort);	 	 			
			
	 return $select;	 	    				
  	}    		
	
	
	/**
    * Save data for new user_log entry.
    *
    * @param  array $data Array of user_log data to insert in database.
    * @return integer|boolean Last inserted log id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function addToLog($lead ,$user_operation) {
        $objSess = new Zend_Session_Namespace(PS_App_Auth);
		$data = array();
		
		switch($user_operation){
			case '1':
					$operation = "Inserted";
				 break;
			case '2':
					$operation = "Updated";
				 break;
			case '3':
					$operation = "Deleted";
				 break;	 	 															 	 
			default:			
					$operation = "";
				break;
	 	}
				
		$data['lead'] = $lead;		
		$data['user_operation'] = $operation;
		$data['user'] = $objSess->name;  			                                
		$data['dcreated'] = date('Y-m-d H:i:s');                                								
																		
        return $this->insert($data);
     }			 	            		

	
}
?>
