<?php

/**
 * Class for Afterschool table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Afterschool extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'afterschool'; 		




	 /**
     * fetch Particular afterschool Detail
     *	 
     */
    /*----------------------| Get spacific afterschool Detail   |----------------------*/
    public function fetchdata($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.id = ?',$id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }



	 /**
     * fetch Particular afterschool Detail
     *	 
     */
    /*----------------------| Get spacific afterschool Detail   |----------------------*/
    public function fetchafterschooldetails($afterschool_id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.id = ?',$afterschool_id);
				
		
		//echo $select; exit;
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }




	 /**
     * Fetch All afterschool
     *	 
     */
    /*----------------------| Get All afterschool Detail   |----------------------*/
    public function fetchafterschoolData($state_code,$cityname) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'afterschool'),array('a.*'))								
				->where('a.state = ?',$state_code)
				->where('a.city = ?', $cityname)
				->where('a.approved = ? ', 1);
				
		$select->order("name asc");
		$select->order("location asc");
		$select->order("address asc");
		
		//echo $select; exit;
		
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }


	/**
    * Save data for new afterschool entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted afterschool id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
		
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		
		//$data['dmodified'] = date('Y-m-d H:i:s');  
		//$data['dcreated'] = date('Y-m-d H:i:s');                        
        //_pr($data,1);
		
        return $this->insert($data);
     }


    /**
     * Update data for afterschool entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }



    /**
     * Update data for afterschool entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateafterschoolData(array $data,$afterschool_id) { 
		$where = "id = " . $afterschool_id;
		//echo $where; exit;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
		
		//_pr($data,1);						
		return $this->update($data,$where);
    }


			 	     
	/**
     * Delete data from afterschool entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
  	public function deleteData($id) {
    	$objData = $this->fetchRow('id='.$id);	
		if(!empty($objData)){
    		$objData->delete();
    	}	
    	unset($objData);
  	}



}
?>
