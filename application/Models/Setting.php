<?php
/**
 * Class for settings table related database operations.
 *
 * @category   NIC
 * @package    Models_Setting
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Setting extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */
    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'ava_settings'; 
	
	/**
     * Get all AVA Setting listing.
     * @param  
     * No param
     * @return array
     */    	
	/*----------------------| fetch all data from searching parameters  |---------------*/		
	public function getAvaSettingList()
	{ 
		$result =	$this->fetchAll()->toArray();
		return $result ;
  	}

    /**
     * Save data for update ava setting entry.
     *
     * @param  array $data Array of setting data to update in database.
     * @param  integer $ava_id ava_id id for which you want to update data.
     * @return integer|boolean Last inserted cron id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data) { 
		//$where = "id = " . $ava_id;
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		return $this->update($data);
    }
   
    /**
     * Get Ava setting detail.
     *
     * @param No param
     * @return array
     */   
	/*----------------------| fetch row data by $ava_id  |-----------------------------*/     
    public function fetchEntry() {
        $select = $this->select();
       return $this->fetchRow($select)->toArray();
     }
     
    /**
     * Get Setting for Time Zone conversion process
     *
	 * @param  No Param
     * @return a data on which fetch operation is to be done.
     */ 
    /*----------------------| Get Setting for Time Zone conversion process  |----------------------*/     
    public function getTimezoneData(){
      		
		$select = $this->select();		
		$fstTableName['tSETTINGS'] = 'ava_settings';
		$select->from($fstTableName,array('tSETTINGS.system_timezone'),null);				
    	$select = 	$this->fetchRow($select);
		if($select)
	    	return $select->toArray();
		else
      		return null;
      		      		
    }

    /**
     * Get Setting for Reengage Waiting Time
	 * @param  No Param
     * @return a data on which fetch operation is to be done.
     */     
    public function getReengageWaitingTime()
    {
    	$select = $this->select();		
		$fstTableName['tSETTINGS'] = 'ava_settings';
		$select->from($fstTableName,array('tSETTINGS.reengage_waiting_time'),null);
    	$select = 	$this->fetchRow($select);
		
    	if($select)
	    	return $select->toArray();
		else
      		return null;
    }
}
?>
