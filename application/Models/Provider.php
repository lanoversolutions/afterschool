<?php

/**
 * Class for Provider table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Provider extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'activity_log'; 		
                   

  	/**
     * Get all activity_log and afterschool_log listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 //$cond_string = "a.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select1 = $this->select();
	 $select1->setIntegrityCheck(false)	 			
				->from(array('a'=>'activity_log'),array('a.id','a.name','a.address','a.email','a.provider_status','a.category'))
				->where('a.user_id = '.$user_id);
	
	$select2 = $this->select();
	$select2->setIntegrityCheck(false)	 			
				->from(array('af'=>'afterschool_log'),array('af.id','af.name','af.address','af.email','af.provider_status','af.category'))
				->where('af.user_id = '.$user_id);
	
	$select = $this->select()->union(array($select1, $select2), Zend_Db_Select::SQL_UNION_ALL)
				->order('id ASC');
		
					 	 	 				 
	if($search_txt != '' && $search_type != '')
		$select->where($cond_string);
	 
	if(trim($sortby)=='');
	
	else
		$select->order($strSort);
	
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
 
 
 
 
 		 /**
     * Fetch Particular activity_log Detail
     *	 
     */
    /*----------------------| Get spacific activity_log Detail   |----------------------*/
	
	public function fetchcategory($user_id)
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'activity_log'),array('a.category'))								
				->where('a.user_id = '.$user_id);
				
		$select = $this->fetchAll($select);
		
		if ($select){
				return $select = $select->toArray();
		}else{
			return null;
	   }
	}

 
  	
}
?>
