<?php
  
    return array(
		
		/*----------------| Admin Section |---------------------*/
				
	
			
    	/*----------------|COMMON ACTION LABEL & FLAG MESSAGE SECTION |-----------*/
	    	"LOGIN_SUCCESS" => "LOGIN SUCCESS"
	    	,"ADMIN_ACTION_LABEL_ADD" => "ADD"
			,"ADMIN_ACTION_LABEL_EDIT" => "EDIT"
			,"ADMIN_ACTION_LABEL_UNDO" => "UNDO"
			,"ADMIN_ACTION_LABEL_SELECT" => "SELECT"
			,"ADMIN_ACTION_LABEL_DELETE" => "DELETE"
			,"ADMIN_ACTION_LABEL_REPORT_FROM" => "FROM"
			,"ADMIN_ACTION_LABEL_REPORT_TO" => "TO"
			
		
		/*----------------| Common Section |---------------------*/
			,"ADMIN_LABEL_USER_STATUS_ACTIVE" => "ACTIVE" 
			,"ADMIN_LABEL_USER_STATUS_NOT_ACTIVE" => "NOT ACTIVE" 
			,"ADMIN_LABEL_ACTION" => "Action"
    	
    	
    	/*----------------| HEADER SECTION |---------------------*/
			,"ADMIN_LABEL_HUY_VO_PROPERTY" => "After School Programs"
	    	,"ADMIN_LABEL_HEADING" => "PROPERTY"    	    	    	
	    	,"ADMIN_LABEL_LOGOUT" => "Logout"
	    	,"ADMIN_LABEL_WELCOME" => "WELCOME"
			,"ADMIN_LABEL_LANDLORD"=>"Landlord Manage"
			,"ADMIN_LABEL_TENANT"=>"Tenant Manage"
			,"ADMIN_LABEL_LANDLOARDINDEX"=>"Landlord Listing"
			,"ADMIN_LABEL_TENANTINDEX"=>"Tenant Listing"
			,"ADMIN_LABEL_USER"=>"User Manage"
			,"ADMIN_LABEL_CONTACT" => "Contact"

			
		/*----------------| PAGINATION SECTION |---------------------*/	
	    	,"LABEL_PAGING_PREVIOUS" => "PREVIOUS"	
	    	,"LABEL_PAGING_NEXT" => "NEXT"	
    	
		
		/*----------------| TOP MENU SECTION |---------------------*/
	    	,"ADMIN_MENU_LABEL_DASHBOARD" => "DASHBOARD"		
			,"ADMIN_MENU_LABEL_USER" => "USER"
			,"ADMIN_MENU_LABEL_RECEPTION" => "RECEPTION"
			,"ADMIN_MENU_LABEL_CUSTOMER" => "CUSTOMER"
			,"ADMIN_MENU_LABEL_USERLOG" => "USERLOG"	
		
		/*------------------------|COMPETITOR TYPE|----------------------*/	
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_SUPERIORS" => "SUPERIORS"
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_PEERS" => "PEERS"
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_SUBORDINATES" => "SUBORDINATES"
		
	    	
		/*----------------| LOGIN SECTION |---------------------*/    	    	
	    	,"ADMIN_LABEL_PAGETITLE_LOIGN" => "AFTERSCHOLL ADMIN" 
			,"USER_LABEL_PAGETITLE_LOIGN" => "USER LOGIN"    	
	    	,"ADMIN_LABEL_HEADING_LOGIN" => "PROPERTY ADMIN"
	     	,"USER_LABEL_HEADING_LOGIN" => "PROPERTY USER LOGIN"	
	    	,"ADMIN_FORM_LABEL_LOGIN_EMAIL" => "EMAIL"
	    	,"ADMIN_FORM_LABEL_LOGIN_PASSWORD" => "PASSWORD"	    	
			,"ADMIN_FORM_LABEL_LOGIN_LOGINBUTTON" => "LOGIN"
	    	,"ADMIN_LABEL_LOGIN" => "LOGIN"
			
    		
	    	//MESSAGES
    			,"ADMIN_MSG_INVALID_LOGIN" => "INVALID EMAIL OR PASSWORD"
				,"EMPTY_EMAIL" => "Please enter your email"
				,"EMPTY_PASSWORD" => "Please enter your password"	    	    				
    	
		
		/*----------------| DASHBOARD(WELCOME) SECTION |---------------------*/	
    		
			
		,"FRONT_LABEL_HOME"=>"Home"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP1"=>'Property Location'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP2"=>'Property Information'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP3"=>'Amenities and Accessibility'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP4"=>'Add Photos'
		,"FRONT_LABEL_CONTACT" => "Contact"
		,"FRONT_LABEL_HOW_TO_APPLY" => "Apply for Voucher"
		,"FRONT_LABEL_HOW_TO_ABOUT_US" => "About us"
		,"FRONT_LABEL_FIND_PROPERTIES" => ""
		,"FRONT_LABEL_FIND_RENTALS" => "Find-Rentals"
		,"FRONT_LABEL_FIND_LOCAL_TENANT" => "Find Local Tenants"
		,"FRONT_LABEL_PAGETITLE_DASHBOARD" => "Section 8 Housing :- DASHBOARD"

		/*----------------| FRONT FOOTER SECTION |---------------------*/	

		,"FRONT_LABEL_ABOUT_US" => "About us"
		,"FRONT_LABEL_TERMS"=>"Terms and Conditions"
		,"FRONT_LABEL_PRIVANCY"=>"Privacy Policy"
		,"FRONT_LABEL_HELP" => "Help"

		/*----------------| USER SECTION |---------------------*/
		
			### INDEX ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_USER" => "USER LIST"
		    	,"ADMIN_LABEL_HEADING_USER" => "USER LIST"
    	
			### ADD ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_USER_ADD" => "USER ADD"
		    	,"ADMIN_LABEL_HEADING_USER_ADD" => "USER ADD"		
    			
    		### UPDATE ACTION
		    	,"ADMIN_LABEL_PAGETITLE_USER_EDIT" => "USER EDIT"
		    	,"ADMIN_LABEL_HEADING_USER_EDIT" => "USER EDIT"
		
			//LABELS 
				,"ADMIN_LABEL_USER_USERNAME" => "USERNAME"
		    	,"ADMIN_LABEL_USER_PASSWORD" => "PASSWORD"
				,"ADMIN_LABEL_USER_NAME" => "NAME"
				,"ADMIN_LABEL_USER_EMAIL" => "EMAIL"
				,"FRONT_BUTTON_LABEL_REGISTRATION" => "Don't have an account? Create one for free"			
		
			//BUTTONS
				,"ADMIN_BUTTON_LABEL_USER_ADD" => "ADD USER"
				,"ADMIN_BUTTON_LABEL_USER_EDIT" => "SAVE CHANGES"
				,"ADMIN_BUTTON_LABEL_USER_SEARCH" => "SEARCH"
					
	    	//LINKS
		    	,"ADMIN_LINK_LABEL_USER_ADD" => "ADD USER"
				,"ADMIN_LINK_LABEL_USER_EDIT" => "EDIT USER"
				,"ADMIN_LINK_LABEL_USER_DELETE" => "DELETE USER"
				,"ADMIN_LINK_LABEL_USER_BACK" => "BACK"
			 		     			
			//MESSAGES
		    	,"ADMIN_MSG_VALID_USER_INSERT" => "RECORD INSERTED SUCCESSFULLY"
				,"ADMIN_MSG_VALID_USER_UPDATE" => "RECORD UPDATED SUCCESSFULLY"    	
		    	,"ADMIN_MSG_VALID_USER_DELETE" => "RECORD DELETED SUCCESSFULLY"	    	
		    	,"ADMIN_MSG_VALID_USER_NO_SELECT_DELETE" => "PLEASE SELECT RECORD(S) TO DELETE"	
	    	
				,"ADMIN_MSG_INVALID_USER_USERNAME" => "USERNAME CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_USER_PASSWORD" => "PASSWORD CAN NOT BE EMPTY!"		
				,"ADMIN_MSG_INVALID_USER_NAME" => "NAME CAN NOT BE EMPTY!"		
				,"ADMIN_MSG_INVALID_USER_EMAIL" => "EMAIL CAN NOT BE EMPTY!"				
				,"ADMIN_MSG_INVALID_USER_SEARCH" => "SEARCH QUERY CAN NOT BE EMPTY!"		
				
			//ALERT MESSAGE 
				,"ADMIN_MSG_ALERT_USER_DELETE" => "DO YOU WANT TO DELETE USER DETAIL?"


			/*----------------|  Account activation Section |---------------------*/

		
			,"ADMIN_LABEL_PAGETITLE_ACTIVATIONACCOUNT_EDIT"=> "Account activation"
			,"ADMIN_MSG_VALID_ACTIVATIONACCOUNT_UPDATE"=>"Your account activated successfully"
			
			
							
			
			/*----------------| ForgotPassword SECTION |---------------------------*/	
				
				,"FORM_LABEL_USER_EMAIL"=>"Email"
				,"FORGOT_PASSWORD_LABEL"=>"ForgotPassword"
				,"ADMIN_BUTTON_LABEL_SEND"=>"Send"
				,"EMAIL_ADDRESS_NOT_EXIST"=>"Email address not exist"
				,"FORGOTPASS_MAIL_SEND" => "Please check your email to get an new Password link."
				,"ADMIN_MSG_VALID_NEWPASS" => "Plaese Enter your new password"
				,"ADMIN_MSG_VALID_PASSWORD_RESET" => "Password sucessfully reset"
				,"ADMIN_LABEL_PAGETITLE_RESETPASSWORD" => "Reset Your Password"
				,"ADMIN_LABEL_PAGETITLE_CHECKPASS" => "Check your Password"
								
		/*----------------| MODULE LIST |---------------------------*/
			### LIST MODULES
				,"ADMIN_LABEL_MODULE_NAME_11_1" => "GLOBAL SETTINGS"
			
				,"ADMIN_LABEL_MODULE_NAME_12" => "USERS"
				,"ADMIN_LABEL_MODULE_NAME_13" => "FRONT USERS"
		
		/*----------------| GENERAL SETTING SECTION |---------------------*/
			### INDEX ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS" => "GENERAL SETTINGS LIST"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS" => "GENERAL SETTINGS LIST"
		    	
				### ADD ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS_ADD" => "GENERAL SETTINGS ADD"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS_ADD" => "GENERAL SETTINGS ADD"		
		    			
		    	### UPDATE ACTION
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS_EDIT" => "GENERAL SETTINGS EDIT"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS_EDIT" => "GENERAL SETTINGS EDIT"
		
		
			//LABELS 
				,"ADMIN_LABEL_GENERALSETTINGS_SITE_TITLE" => "SITE TITLE"
		    	,"ADMIN_LABEL_GENERALSETTINGS_EMAIL_ID" => "EMAIL ID"
		    	,"ADMIN_LABEL_GENERALSETTINGS_CONTACT_NO" => "CONTACT NO"
		    	,"ADMIN_LABEL_GENERALSETTINGS_CONTACT_ADDRESS" => "CONTACT ADDRESS"
				,"ADMIN_LABEL_GENERALSETTINGS_PAGING_VARIABLE" => "PAGING VARIABLE"
		     	,"ADMIN_LABEL_GENERALSETTINGS_PAGE_IN_GROUP" => "PAGE IN GROUP"
				,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE1" => "MASSAGE1 TEXT"
		     	,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE2" => "MASSAGE2 TEXT"
				,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE3" => "MASSAGE3 TEXT"
			
			
			//BUTTONS
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_ADD" => "SUBMIT"
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_EDIT" => "EDIT GENERALS SETTING"
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_SEARCH" => "SEARCH"
						
	    	//LINKS
		    	,"ADMIN_LINK_LABEL_GENERALSETTINGS_ADD" => "ADD GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_EDIT" => "EDIT GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_DELETE" => "DELETE GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_BACK" => "BACK"
			 		     			
			//MESSAGES
		    	,"ADMIN_MSG_VALID_GENERALSETTINGS_INSERT" => "RECORD INSERTED SUCCESSFULLY"
				,"ADMIN_MSG_VALID_GENERALSETTINGS_UPDATE" => "RECORD UPDATED SUCCESSFULLY"    	
		    	,"ADMIN_MSG_VALID_GENERALSETTINGS_DELETE" => "RECORD DELETED SUCCESSFULLY"	    	
		    	
		    	,"ADMIN_MSG_INVALID_GENERALSETTINGS_LABID" => "LAB ID CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_NAME" => "NAME CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_ADDRESS" => "ADDRESS CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_PHONE_NO" => "PHONE NO CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_SEARCH" => "SEARCH QUERY CAN NOT BE EMPTY!"		
					
			//ALERT MESSAGE 
				,"ADMIN_MSG_ALERT_GENERALSETTINGS_DELETE" => "DO YOU WANT TO DELETE GENERALS SETTING DETAIL?"	
		
		
		/*----------------| Login Section |---------------------*/
		
		,"FRONT_LABEL_LOGIN"=>"User Login"
		
		
  		/*----------------| REGISTRATION USER Section |---------------------*/
			### index Action	
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER" => "User Register List"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER" => "User Register List"
				,"ADMIN_LABEL_PAGETITLE_USER_DETAILS" => "User Details"
				,"FRONT_LABEL_REGISTRATION"=> "User Registration"
	    	
			### Add Action	
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_ADD" => "User Registration Settings Add"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER_ADD" => "User Registration Settings Add"
	    			
	    	### Update Action
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_EDIT" => "User Profile"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER_EDIT" => "User Profile"
		
		
			//labels 
				,"ADMIN_LABEL_REGISTRATION_FIRST_NAME" => "First Name"
		    	,"ADMIN_LABEL_REGISTRATION_LAST_NAME" => "Last Name"
				,"ADMIN_LABEL_REGISTRATION_EMAIL" => "Email"
				,"ADMIN_LABEL_REGISTRATION_CONFIRM_EMAIL" => "Confirm Email"
				,"ADMIN_LABEL_REGISTRATION_PASSWORD" => "Password"
				,"ADMIN_LABEL_REGISTRATION_CONFIRM_PASSWORD" => "Confirm Password"
				,"ADMIN_LABEL_REGISTRATION_STATUS"=>"Status"
				,"ADMIN_LABEL_REGISTRATION_OPTION"=>"User Option"
				,"ADMIN_LABEL_REGISTRATION_ACTIVE"=>"Active"
				,"ADMIN_LABEL_REGISTRATION_INACTIVE"=>"Inactive"
				,"ADMIN_LABEL_USER_OPTION_LANDLORD"=>"Landlord"
				,"ADMIN_LABEL_USER_OPTION_TENANT"=>"Tenant"
				,"ADMIN_LABEL_REGISTRATION_OLD_PASSWORD" => "Old Password"
				,"ADMIN_LABEL_REGISTRATION_NEW_PASSWORD" => "New Password"
				,"ADMIN_LABEL_REGISTRATION_RETYPE_PASSWORD" => "Re Type Password"
				,"ADMIN_LABEL_REGISTRATION_TYPE" => "USer Type"
		
			//buttons
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_ADD" => "Continue"
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_EDIT" => "Edit"
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_SEARCH" => "Search"
				,"ADMIN_BUTTON_LABEL_CHANGE_PASSWORD" => "Change Password"
						
		    //links
		    	,"ADMIN_LINK_LABEL_REGISTRATIONUSER_ADD" => "Add User Registration"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_MANAGE" => "Manage User"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_DELETE" => "Delete User Registration"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_BACK" => "Back"
				 		     			
			//Messages
		    	,"ADMIN_MSG_VALID_REGISTRATIONUSER_INSERT" => "Registration Successfully. You need to Activate Your account. Please check your email to get an activation link."
				,"ADMIN_MSG_VALID_REGISTRATIONUSER_UPDATE" => "Details Updated Successfully"    	
		    	,"ADMIN_MSG_VALID_REGISTRATIONUSER_DELETE" => "Record Deleted Successfully"	    	
		    	
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_NAME" => "Name can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ADDRESS" => "Address can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PHONE_NO" => "Phone no can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_SEARCH" => "Search Query can not be empty!"
		
					
/*				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ALLREADY_TAKEN" => "User name or Email address allready taken"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NO_MATCH" => "Password must not matched"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NO_BLANK" => "Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME_NO_BLANK" => "First Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NO_BLANK" => "Email address must not not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NO_MATCH" => "Email address must not matched"				
*/				
				,"FRONT_MSG_INVALID_LOGIN" => "Please Login First"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME"=>"First Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_LAST_NAME"=>"Last Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL"=>"Email address must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_EMAIL"=>"Confirm Email address must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD"=>"Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_PASSWORD"=>"Confirm Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ALLREADY_TAKEN"=>"Email Address Already taken"
				,"ADMIN_MSG_INVALID_PASSWORD" => "Old password does not match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_OLD_PASSWORD" => "Old password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_NEW_PASSWORD" => "New password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_RETYPE_PASSWORD" => "Re-type password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NOT_MATCH" => "Email or Confirm Email do match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NOT_MATCH" => "Password or Confirm Password do not match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_INVALID_EMAIL_FORMATE" => "Invalid email address"
				,"ADMIN_MSG_INVALID_PASSWORD_NOT_MATCH" => "Password or Retype Password do not match"
			
			//alert message 
				,"ADMIN_MSG_ALERT_REGISTRATIONUSER_DELETE" => "Do you want to delete User Registration detail?"	
	
	
			/*----------------| SITE TITLE Section |---------------------*/
			
			,"FRONT_LABEL_PAGETITLE_INDEX" => "After School Care | After School Activities | Summer Camps | After School Programs"


		  /*----------------| Contact Section |---------------------*/
			,"ADMIN_LABEL_PAGETITLE_CONTACT" => "Contact | After School Programs"
			
			//lable
			
			,"ADMIN_LABEL_CONTACT_NAME" => "Your Name"
			,"ADMIN_LABEL_CONTACT_EMAIL" => "Your e-mail address"
			,"ADMIN_LABEL_CONTACT_SUBJECT" => "Subject"
			,"ADMIN_LABEL_CONTACT_MESSAGE" => "Message"
			,"ADMIN_LABEL_CONTACT_CAPTCHA" => "Please type each of the following words into the box below to continue"
			,"ADMIN_BUTTON_LABEL_CONTACT_ADD" => "Send e-mail"
				


		  /*----------------| Welcome Section |---------------------*/
			,"ADMIN_LABEL_ACTIVITY" => "Activity Log"
			,"ADMIN_LABEL_AFTERSCHOOL" => "Afterschool Log"
			,"ADMIN_LABEL_ACTIVITY_HOME" => "Activity"
			,"ADMIN_LABEL_AFTERSCHOOL_HOME" => "Afterschool"
			,"ADMIN_LABEL_REVIEW" => "Review"
			
			
			
			/*----------------| Activity Section |---------------------*/
			,"ADMIN_LABEL_PAGETITLE_ACTIVITY" => "Activity Dashboard"
			,"FRONT_LABEL_PROVIDER_ACTION" => "Action"
			,"ADMIN_LABEL_PAGETITLE_ACTIVITY_DETAILS" => "Activity Details"
			
			,"ADMIN_LABEL_PENDING" => "Pending"
			,"ADMIN_LABEL_APPROVED" => "Approved"
			,"ADMIN_MSG_VALID_SAVED" => "Saved Successfully"
			
			/*----------------| Activity Section |---------------------*/
			,"ADMIN_LABEL_PAGETITLE_AFTERSCHOOL" => "Afterschool Dashboard"
			,"ADMIN_LABEL_PAGETITLE_AFTERSCHOOL_DETAILS" => "Afterschool Details"
						
			
			/*----------------| Reviews Section |---------------------*/
			
			,"FRONT_BUTTON_LABEL_REVIEW_ADD" => "Add Review"
			,"ADMIN_LABEL_PAGETITLE_REVIEW" => "Reviews Dashboard"	
			
			,"ADMIN_LABEL_PAGETITLE_REVIEW_DETAILS" => "Review Details"
			,"FRONT_LABEL_REVIEW_EMAIL" => "Email address (will not be published)"
			,"FRONT_LABEL_REVIEW_NAME" => "Display Name"
			,"FRONT_LABEL_REVIEW_RATING" => "Rating (1=poor, 5=exellent)"
			,"FRONT_LABEL_REVIEW_MESSAGE" => "Write your comment"
			,"FRONT_LABEL_REVIEW_CAPTCHA" => "Please type each of the following words into the box below to continue"
			,"ADMIN_LABEL_PROVIDER_RATING" => "Rating"
			,"ADMIN_LABEL_PROVIDER_COMMENTS" => "Comments"
			,"ADMIN_LABEL_PROVIDER_PROVIDER_NAME" => "Provider Name"
			,"ADMIN_LABEL_PROVIDER_TABLE" => "Table"
			
			,"ADMIN_MSG_INVALID_CONTACT_NAME" => "Display Name must not Blank"
			,"ADMIN_MSG_INVALID_CONTACT_EMAIL" => "Email address must not Blank"
			,"ADMIN_MSG_INVALID_REVIEW_RATING" => "Rating must not Blank"
			,"ADMIN_MSG_INVALID_REVIEW_MESSAGE" => "comment must not Blank"
			,"ADMIN_MSG_INVALID_CONTACT_SUBJECT" => "Subject must not Blank"
			,"ADMIN_MSG_INVALID_CONTACT_MESSAGE" => "Message must not Blank"


			/*----------------| Acccount Section |---------------------*/
			
			,"FRONT_LABEL_PAGETITLE_ACCOUNT_ADD" => "Select User Option"
			
			
			,"FRONT_MSG_INVALID_ACCOUNT_OPTION" => "PLease select User Option"
			,"FRONT_BUTTON_LABEL_ACCOUNT_ADD" => "Please select any option"
			
			,"FRONT_MSG_VALID_ACCOUNT_INSERT" => "Saved Successfully"
			
			,"FRONT_LABEL_PROPERTY_USER_OPTION" => "User Option"
						
  
  		/*----------------| DASHBOARD Section |---------------------*/
		
		,"FRONT_LINK_LABEL_DASHBOARD_AFTERSCHOOL_ACTIVITY" => "Add Activity"
		,"FRONT_LINK_LABEL_DASHBOARD_MARTIALARTS" => "Martial Arts"
		,"FRONT_LINK_LABEL_DASHBOARD_YOUTHSPORTS" => "Youth Sports"
		,"FRONT_BUTTON_LABEL_EDIT" => "Edit Details"
		,"FRONT_LINK_LABEL_CHANGE_PASSWORD" => "Change Password"
		,"FRONT_LINK_LABEL_MY_REVIEWS" => "My Reviews"
 			
  		
		/*----------------| PROVIDER Section |---------------------*/
		### index Action	
    	,"FRONT_LABEL_PAGETITLE_PROVIDER" => "Provider Add"
    	,"FRONT_LABEL_HEADING_PROVIDER_LIST" => "Provider Add"
    	
		### Add Action	
    	,"FRONT_LABEL_PAGETITLE_PROVIDER_ADD" => "Category Add"
    	,"FRONT_LABEL_HEADING_PROVIDER_ADD" => "Category Add"		
    			
    	### Update Action
    	,"FRONT_LABEL_PAGETITLE_PROVIDER_EDIT" => "Category Edit"
    	,"FRONT_LABEL_HEADING_PROVIDER_EDIT" => "Category Edit"
		
		//labels 
		,"FRONT_LABEL_PROVIDER_NAME" => "Name"	
		,"FRONT_LABEL_PROVIDER_ADDRESS" => "Address"
		,"FRONT_LABEL_PROVIDER_CITY" => "City"
		,"FRONT_LABEL_PROVIDER_STATE" => "State"
		,"FRONT_LABEL_PROVIDER_ZIP" => "Zip"
		,"FRONT_LABEL_PROVIDER_CATEGORY" => "Category"
		,"FRONT_LABEL_PROVIDER_NOTE" => "Note"
		,"FRONT_LABEL_PROVIDER_PHONE" => "Phone"
		,"FRONT_LABEL_PROVIDER_URL" => "Url"
		,"FRONT_LABEL_PROVIDER_FILENAME" => "Filename"
		,"FRONT_LABEL_PROVIDER_EMAIL" => "Email"
		,"FRONT_LABEL_PROVIDER_DETAILS" => "Details"
		,"FRONT_LABEL_PROVIDER_KIDS_PROGRAM_URL" => "Kids Program Url"
		,"FRONT_LABEL_PROVIDER_SUMMER_PROGRAM_URL" => "Summer Program Url"
		,"FRONT_LABEL_PROVIDER_VIDEO_URL" => "Video Url"
		,"ADMIN_LABEL_STATES_SELECT" => "Please select state"
		,"FRONT_LABEL_PROVIDER_LOCATION" => "Location"
		,"FRONT_LABEL_PROVIDER_ADDRESS2" => "Address2"
		,"FRONT_LABEL_PROVIDER_CAPACITY" => "Capacity"
		,"FRONT_LABEL_PROVIDER_TYPE" => "Type"
		,"FRONT_LABEL_PROVIDER_AGE_RANGE" => "Age Range"
		,"FRONT_LABEL_PROVIDER_INTRODUCTION" => "Introduction"
		,"FRONT_LABEL_PROVIDER_WEBSITE" => "Website"
		,"FRONT_LABEL_PROVIDER_STATUS" => "Status"
		,"FRONT_LINK_LABEL_ADD_MORE" => "Add More Images"
		,"FRONT_LINK_LABEL_IMAGES" => "Image"
		,"FRONT_LABEL_PROVIDER_CLAIM" => "Claim"
		
    	
		//buttons
		,"FRONT_BUTTON_LABEL_PROVIDER_ADD" => "Add Provider"
		,"FRONT_BUTTON_LABEL_PROVIDER_EDIT" => "Save"
		,"FRONT_BUTTON_LABEL_PROVIDER_SEARCH" => "Search"
				
    	//links
    	,"FRONT_LINK_LABEL_PROVIDER_ADD" => "Add Category"
		,"FRONT_LINK_LABEL_PROVIDER_EDIT" => "Edit Category"
		,"FRONT_LINK_LABEL_PROVIDER_RELATION_ADD" => "Add"
		,"FRONT_LINK_LABEL_PROVIDER_MANAGE" => "Manage"
		,"FRONT_LINK_LABEL_PROVIDER_DELETE" => "Delete Category"
		,"FRONT_LINK_LABEL_PROVIDER_BACK" => "Back"
			    	
		### Add Action	
    	,"FRONT_LABEL_PROVIDERTITLE_PROVIDER_ADD" => "Provider Add"
    	,"FRONT_LABEL_HEADING_PROVIDER_ADD" => "Provider Add"
		
		### Update Action
    	,"FRONT_LABEL_PROVIDERTITLE_PROVIDER_EDIT" => "Provider Edit"
    	,"FRONT_LABEL_HEADING_PROVIDER_EDIT" => "Provider Edit"				
		
		//Messages
    	,"FRONT_MSG_VALID_PROVIDER_INSERT" => "Saved Successfully"
		,"FRONT_MSG_VALID_PROVIDER_UPDATE" => "Saved Successfully"    	
    	,"FRONT_MSG_VALID_PROVIDER_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_ALERT_IMAGES_DELETE" => "Do you want to delete Image?"
		,"FRONT_MSG_INVALID_PROVIDER_PROVIDER" => "Category can not be empty!"				
		,"FRONT_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_NAME" => "Name can not be empty!"			
		,"FRONT_MSG_INVALID_PROVIDER_ADDRESS" => "Address can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_CITY" => "City can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_STATE" => "State can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_ZIP" => "Zip can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_CATEGORY" => "Category can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_NOTE" => "Note can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_PHONE" => "Phone can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_URL" => "Url can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_FILENAME" => "Filename can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_EMAIL" => "Email can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_DETAILS" => "Details can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_KIDS_PROGRAM_URL" => "Kids Program Url can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_SUMMER_PROGRAM_URL" => "Summer Program Url can not be empty!"
		,"FRONT_MSG_INVALID_PROVIDER_VIDEO_URL" => "Video Url can not be empty!"
		
				
		//alert message 
		,"FRONT_MSG_VALID_PROVIDER_IMAGES_DELETE" => "You do not have privilege to delete this image" 
		,"FRONT_MSG_ALERT_PROVIDER_DELETE" => "Do you want to delete Provider detail?"
		,"ADMIN_MSG_SUCCESS_IMAGE_DELETED" => "Image deleted successfully"
		
		
   );
?>




